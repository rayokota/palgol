## Introduction

Palgol is a domain-specific language for large-scale graph processing.
Palgol stands for (**P**regel **Algo**rithmic **L**anguage), which provides a high-level interfaces for programmers to concisely describe various **vertex-centric** graph algorithms; the Palgol compiler then transforms the program to efficient code on a Pregel-like system.
Here, "vertex-centric" refers to the "think like a vertex" paradigm, which has already shown its power to parallelize various graph algorithms to handle large-scale graphs.

This is a tutorial of the Palgol language.
Since Palgol is a domain-specific language, some basic knowledge of parallel programming and graph processing will help you better understand this language, which are:

1. Bulk Synchronous Parallel (BSP) Model
2. Pregel: A System for Large-Scale Graph Processing

Even if you are not familiar with them, we will give explanation when we come across these concepts, hopefully making this tutorial self-contained and easy to understand.

Palgol currently compiles to [Pregel+](http://www.cse.cuhk.edu.hk/pregelplus/), an open-sourced Pregel system written in C++.
However, it has the potential to be executed much faster using a more advanced Pregel implementation, which is now an ongoing project of my own.


## 1. Syntax and Semantics

### 1.1 Palgol Step

#### 1.1.1 The Vertex-Centric Computation

The first thing that needs to be mentioned is the vertex-centric graph algorithms, which are significantly different from those traditional ones.
The most important difference is that, a vertex-centric algorithm specifies the computation that performs on **every** vertex!

There is a good reason to define the computation in the vertex-centric way:
to make the graph algorithm easy to parallelize or scale.
If every vertex performs the same computation, then we can simply distribute the vertices of a graph to a bunch of processors, with each processor sequentially executing the same code using different vertex's state.
In additional to the vertex-centric way of specifying computation, the communication also plays an important role to implement various graph algorithms, but so far we can just focus on the computing.

In Palgol, we have an important concept called **Palgol step**, which represents a basic vertex-centric computation.
Here is a very simple example of a Palgol step:

```
// enter vertex-centric mode
for u in V
  // a simple assignment
  Active[u] := true
end
```
This program contains a single _for loop_ and an assignment.
Let us first look at the for loop `for u in V .. end`.
Recall that a vertex-centric algorithm specifies the computation that perform on every vertex, and this for loop has exactly the same meaning: the keyword **V** represents all the vertices in the graph, and the for loop binds each vertex to the variable _u_.
Then, inside the loop body, we can use the variable _u_ to refer to that vertex, and do anything to manipulate the vertex's state.

It is guaranteed by Palgol that there is no data dependency between vertices in a Palgol step, so the order of the vertices does not matter.
In this example, we simply set the field "Active" of every vertex to true (detailed explanation is in Section 1.2).

#### 1.1.2 Indentation Rule

Palgol is an indentation-based language.
Unlike python which forces users to choose either a tab or four spaces for indentation, Palgol's indentation rule is slightly looser:
users only need to ensure that the code in the same block has the same indentation.
In this tutorial, we always use 2 spaces for indentation, since it looks nice and saves spaces.

Someone may notice that, the **end** keyword in Palgol is redundant, because the indentation already tells the compiler where is the end of a code block.
However, we decided to add the keyword **end** to make the program easier to be read by human.
Furthermore, we made **end** keyword optional, so there is no problem to remove it, making the program more concise:

```
for u in V
  Active[u] := true
```

#### 1.1.3 Comments

Comments are notes that you leave to other programmers to help explain things about your code.
Palgol has line comments, which are anything after ‘//’ and extend to the end of the line:

```
for u in V
  // a simple assignment
  Active[u] := true
end
```

Comments are simply ignored by the Palgol compiler.

### 1.2 Local Field Access

In Palgol, we are basically writing programs to handle the vertices' states.
In particular, we use the _global array access_ syntax to read or modify the vertices' states.
Let us look at this example:

```
for u in V
  Active[u] := Degree[u] > 0
end
```
The assignment inside the loop body assigns true to the "Active" field of a vertex, only if the vertex's "Degree" attribute is larger than 0.
Here, "Active" and "Degree" are just user-specified field names, and they represent the memory space to store values (a boolean and an integer in this example) for each vertex.
As mentioned before, _u_ is binded to the vertices of the graph, then `Active[u]` and `Degree[u]` represent the local field "Active" and "Degree" of the current vertex.

In Palgol, a field name always starts with a capital letter, which is to distinguish from the variables that starts from a lowercase letter (variables will be introduced in section 1.4).
Let us see an example:

```
for u in V
  Active[u] := Degree[u] > 0 // correct
  inactive[u] := false       // compiling error
end
```

In this case, the compiler finds that the third line has a field name ('inactive') starting with a lowercase letter, then it reports a compiling error in line 3.

### 1.3 Input and Output

Up to now, we have introduced a simple example of Palgol program that calculates the "Active" field according to the "Degree" of a vertex:

```
for u in V
  Active[u] := Degree[u] > 0
end
```

Suppose we save this file as `example.pal`, we can compile this program using the following command:

```
$ ./palgol example.pal
```
However, we will get a compiling error like this:

```
line 2.16 error: field 'Degree' has not been initialized
  Active[u] := Degree[u] > 0
               ^
```

The error message imposes a question to us: where does the field "Degree" come from?
Obviously, this simple piece of program doesn't provide any information about this.
There are usually two possibilities: either "Degree" is a field read from the input graph, or it is not properly initialized by the programmer.
Since using a field without proper initialization is dangerous, Palgol always forces the users to provide the information of input fields, so that the compiler can report every possible uninitialized fields.

In Palgol, we provide a simple interface for users to specify the input graph.
We just add an additional line at the beginning of the program:

```
input[Id, Degree]

for u in V
  Active[u] := Degree[u] > 0
end
```

In this example, we tell the compiler that two fields are read from the input graph: the "Id" field and the "Degree" field.
The "Id" field is a special one that stores the unique identifier of each vertex (required by the Pregel system, which has integer type), while the "Degree" is a user-specified field.
We can put any number of fields inside the brackets.

One convenient feature of Palgol is that, the compiler automatically generates a parser to read those fields from the input graph. (**TODO**)

We can also specify the output fields of this program.
The syntax is similar to specifying the input fields:

```
input[Id, Degree]
output[Id, Active]

for u in V
  Active[u] := Degree[u] > 0
end
```
We just add a new line starting with keyword **output**, and attach a list of field names after it as output fields.
Then, the compiler will generate a printer to output those fields in the default format.

Putting `input[Id, ..]` in each example is a bit tedious, so we will leave that out in the rest part of this tutorial.
If you are following along, make sure to specify the input fields, otherwise you need to explicitly make the compiler to ignore the uninitialized fields, by adding a special option "-no-check" in compilation:

```
$ ./palgol -no-check example.pal
```

### 1.4 Variable Binding

In Palgol, in addition to using local fields, we can also use _variable binding_ to create temporary variables:

```
for u in V
  let v = 5
end
```
This is sometimes called _let binding_ in other languages, which is a statement starting from the keyword **let**, followed by a variable name and an expression.

Palgol is a statically typed language, which means that we specify our types up front, and they are checked at compile time.
However, it does not mean you need to specify type everywhere.
Palgol has this thing called type inference.
If it can figure out what the type of something is, Palgol doesn't require you to actually type it out.

We can add the type if we want to, though. Types come after a colon:

```
for u in V
  let v: int = 5
end
```

By default, bindings are immutable. This code will not compile:

```
for u in V
  let v = 5
  v := 10   // compiling error
end
```
The compiler will give you this error:

```
line 3.3 error: variable 'v' is immutable
> use 'let mut v = ..' to enable variable mutation
  v := 10
  ^
```
It also gives you a hint to correct the program, which is adding the keyword **mut** after **let**.
That is exactly how to make a binding mutable in Palgol:

```
for u in V
  let mut v = 5
  v := 10
end
```

There is no single reason that bindings are immutable by default, but we can think it as a guarantee of safety.
If you forget to say **mut**, the compiler will catch it, and let you know that you have mutated something you may not have intended to mutate.
If bindings were mutable by default, the compiler would not be able to tell you this.
If you did intend mutation, then the solution is quite easy: add **mut**.

### 1.5 Types

#### 1.5.1 Basic Types: int, bool, float

Palgol has three basic types, which are **int**, **bool** and **float**.
Here, we simply present how to use these types in Palgol:

```
for u in V
  let x: int   = 5
  let y: float = 5.0
  let z: bool  = false

  let x_err: int = 5.0  // type mismatch
  let y_err: float = 5  // type mismatch
  let z_err: bool = x   // type mismatch
end
```
As introduced before, type signature can be used in variable binding to specify the type of the variable, so that the compiler can report errors if the expression evaluates to something with unexpected type.

Palgol will never do type conversion implicitly.
Instead, users need to explicitly write type conversion in the code like this
(int ~ float, int ~ bool):

```
for u in V
  let x: int   = 5
  let y: float = 5.0
  let z: bool  = false

  let x_from_y: int = to_int y
  let x_from_z: int = (z ? 1 : 0)

  let y_from_x: float = to_float x
  let y_from_z: float = (z ? 1.0 : 0.0)
end
```

In this example, `to_int` and `to_float` are two build-in functions for type conversion between int and float.

#### 1.5.2 Vertex Identifier: vid

In all the examples above, we use `for u in V` to start a code block representing a basic vertex-centric computation, which binds each vertex to variable _u_.
Then, what's the type of _u_?

In Palgol, we give this variable a special type **vid**, which represents a 'reference' to that vertex.
As introduced before, we can do field access from a reference of some vertex:

```
for u in V
  // Degree[u]: field access
  Active[u] := Degree[u] > 0
end
```

The syntax of field access is `FieldName[..]`, and Palgol's type system requires that the expression inside the brackets has **vid** type.
We can assign an expression with **vid** type to a field, as shown in the following example:

```
for u in V
  Ref[u] := u
end
```

In this example, the field `Ref` holds a reference of the current vertex, and therefore has type **vid**.
`Ref` can also store the reference of any other vertex.
Allowing users to store the reference of some vertex in the field can greatly improve the expressiveness of Palgol.
For example, we can write `Active[Ref[u]]` to fetch data from the reference stored in `Ref[u]`.
We call such syntax as _chain access_, which will be introduced in section 1.7.

The references of the vertices (expressions in **vid** type) play an important role in Palgol.
It directly maps to the **vertex identifier** in Pregel systems:
in Pregel, each vertex should have a unique vertex identifier;
then, a vertex can send some messages to another vertex, by providing the vertex identifier of the destination vertex as well as the message to send.
In Pregel system, the vertex identifier is a user-provided type, but in current Palgol, **vid** simply compiles to int.
In the future, we will allow users to specify the type of vertex identifier.

In current Palgol, expressions with **vid** type behave differently from the intergers (**int** type): we can only do field access, assignment or comparison on **vid** type, but we cannot perform arithmetic computation over it:

```
for u in V
  let x: vid = Ref[u]     // assignment OK
  let less: bool = u < x  // comparison OK
  let s = u + x           // type error!!
end
```

Instead, Palgol provides a special immutable field `Id[..]` with type **int** which stores the value of the vertex identifier.
We can rewrite the code above to make it correct:

```
  // operator '+' is performed on two integers
  let s = Id[u] + Id[x]
```

#### 1.5.3 Pairs

**pair** is the simplest compounded type in Palgol that represents a tuple of two elements with any type.
Here is an example to perform variable binding and comparison:

```
for u in V
  let x: (int, float) = (1 + 2, 3.333)
  let y: (int, float) = (6 - 4, 5.555)
  let cmp = (x < y)  // false, since 1 + 2 > 6 - 4
end
```

For comparison, the two operants should have exactly the same type.
In this example, both of x and y are pair type whose first element is **int** and second element is **float**.
The first components are first compared.
According to the result, the second component is compared next.

Pair can be nested, and there is no limitation of the depth of pair.
Besides, we provide two build-in operators **fst** and **snd** to let users access the components of a pair:

```
for u in V
  let v: (int, (bool, float)) = (1, (true, 2.0))
  let x = fst v
  let y = fst (snd v)
  let z = snd (snd v)
end
```

#### 1.5.4 Specialized Pairs

Next, we will introduce a specialized pair type, which provides such abstraction: reference with its corresponding value (e.g., an edge in graph).
As usual, let's start from an example:

```
for u in V
  let a: {vid, int}   = {u, 10}
  let b: {vid, float} = {u, 1.0}
  let c: {vid, (int, bool)} = {u, (1, true)}
  let d: {vid, unit} = {u}
  let e: {vid}       = {u}
end
```

A specialized pair is enclosed by the braces `{ }`, which contains one or two expressions indicating the reference and an optional corresponding value.
The reference always comes before the value.
In this example, we create 5 variables with this special type:
_a_ and _b_ are references with integer and float value, while _c_ is a reference holding a pair of integer and boolean values.
Then, _d_ and _e_ have the same value type called **unit**, indicating there is no value corresponding to the reference.
In Palgol, to describe an edge without corresponding value, we can simply omit the second component, leaving only the reference inside the braces (e.g., `{u}`).
In type level, both of `{vid}` and `{vid, unit}` represent a reference without corresponding value.

We can access the components of such pair as follows:

```
for u in V
  let a: {vid, int} = {u, 10}
  let x: vid = a.ref  // get the reference
  let y: int = a.val  // get the value
end
```

From the examples above, we can see that an edge is something that can be either a **pair** type (with its first component having **vid** type), or a **vid** type (with the second component omitted).
However, we introduce this special pair type for the following reasons:

1. In Palgol, the edges are stored as references (with possibly edge weights) on both of their endpoints, which can be naturally represented by such type. Besides, we ask users to explicitly write '.ref' or '.val' to access the reference or value of the edge, which makes the program easier to read and understand.
2. By using a special type for edges, it is easier to let the compiler to make some assumptions on the edges, so that it can produce more efficient code.

Here, we just present a simple program using the specialized pair type, which calculates the minimum edge weight of the outgoing edges for each vertex, using the 'for loop' (see section 1.8):

```
// 'Out' stores the list of outgoing edges
//       and the inferred type is [{vid, int}]
input[Id, Out]
output[Id, Min]

for u in V
  let mut minVal: int = inf
  for (e <- Out[u])
    if (e.val < minVal)
      minVal := e.val
  Min[u] := minVal
end

```

#### 1.5.5 Arrays

Like many programming languages, Palgol has list type to represent a sequence of objects with the same type.
The most basic is the _array_, a dynamic list of elements of the same type.
By default, arrays are immutable.

```
for u in V
  let     a: [int] = [1, 2, 3]   // an immutable array
  let mut m: [int] = [1, 2, 3]   // a mutable array
  local L[u] := [1, 2, 3]        // fields are always mutable
  
  let size_a = a.size()   // size_a is 3
  m.append(4)             // append 4 to the end of m
  let size_m = m.size()   // size_m is 4
  L[u].append(5)          // append 5 to the end of L[u]
  m := []                 // clear the array m
end
```

You can get the number of elements in an array with ".size()", or append new elements at the end of the array with ".append(..)".
However, current Palgol does not support accessing a particular element in the array.

#### 1.5.6 Type Inference

Palgol is a statically typed language, but it has a thing called type inference to automatically deduce the type of expressions, so that users do not need to declare everything.
To show how type inference works, let's see the following example:

```
input[Id, Value]
output[Id, Result]

for u in V
  let x: (int, (int, bool)) = (Value[u], (0, true))
  Result[u] := x
end
```

In this program, each vertex reads the 'Value' field from the input, creates a nested pair structure (and binds it to variable _x_), and then write it to the 'Result' field.
If we ignore the type signature of _x_, the compiler cannot deduce the type of 'Value', since we just read it from the input and write it to the 'Result' field, without using any other operations that may provide more type information.
Therefore, the type signature of _x_ is important, which helps the compiler to deduce the types of the fields: 

* The first component of _x_ has type **int**, since we have `x: (int, (int, bool))`.
* The expression on the right hand side is a pair with `Value[u]` as its first component, so 'Value' has type **int**.
* The second component of the expression `(0, true)` does match the type `(int, bool)`.
* The assignment `Result[u] := x` indicates that 'Result' has the same type as _x_.

In this example, the only important thing is actually the type of 'Value', and all the other things can be easily inferred.
To make this program more concise, Palgol allows users to write the program like this:

```
input[Id, Value]
output[Id, Result]

for u in V
  let x: (int, _) = (Value[u], (0, true))
  Result[u] := x
end
```

Here the underline in the type signature `(int, _)` is just a placeholder that tells the compiler "please infer the type of the second component of the pair for me".
Users can put any number of placeholders in the type signature, and each one will be inferred independently.

However, someone may still find it unsatisfactory.
The reason is that, we may just want to write this program in this way:

```
input[Id, Value]
output[Id, Result]

for u in V
  Result[u] := (Value[u], (0, true))
end
```

However, in this case, we can no longer annotate the type of 'Value', so that the compiler cannot infer the type of 'Value' or 'Result'.
To handle the cases that Palgol cannot infer all the types, we provide a syntax to let users directly specify the type of a field, which looks more convenient in this case:

```
input[Id, Value]
output[Id, Result]

field Value: int

for u in V
  Result[u] := (Value[u], (0, true))
end
```

The type declaration starts from the keyword `field` and followed by a field name, a colon and a type.
Besides, it should come after the input/output and before the program body.
Moreover, this declaration actually specifies the type of `Value[u]`, but our current syntax is simple and clear enough.

#### 1.5.7 Type Alias

Sometimes, it may be tedious to write some long type again and again.
Palgol provides the syntax of type alias, which allows you to use a name to refer to a previously defined type:

```
type edgeList = [{vid, (int, int)}]

field List1: edgeList
field List2: edgeList
```

The type alias should come before the first place you use that name.

### 1.6 Chain Access

Chain access is the first remote access pattern introduced in this tutorial, and is a unique feature of Palgol.
Here, a remote access pattern means reading the field of a vertex other than itself, through the reference.

Briefly speaking, chain access is a special class of expression in the form of `A[B[..X[u]..]`, in which _u_ is the current vertex, and all the fields involved (except the outermost one) have **vid** type.
Besides, the depth of the chain access is arbitrary, and you can use a field one more time in a chain access expression.

Chain access is capable of describing really complex computation patterns, however it is extremely easy to use.
Let's think about this scenario:
Suppose you have a forest where each vertex holds a reference to the parent vertex (and root vertices point to themselves).
Now you want every vertex to know who is its grandparent (the parent of its parent).
This is exactly the example that chain access is good at:

```
input[Id, Parent]
output[Input, GranP]

for u in V
  GranP[u] := Parent[Parent[u]]
end
```

In this example, the expression `Parent[Parent[u]]` accesses _u_'s parent's parent reference, and stores it in a new field "GranP" which is short for grandparent.
This piece of code is easy to understand, clear, well-typed, while in contrast, its Pregel implementation contains 3 supersteps implementing the request-response conversation between every vertex and its parent.
The compilation of chain access to message passing is quite a technical problem, and you can refer to the [technical report](https://arxiv.org/abs/1703.09542) on arXiv for more details, if you are interested in the implementation.

### 1.7 List Comprehension

List comprehension is a very useful expression in Palgol.
The syntax itself is actually borrowed from Haskell for concisely generating lists.
Let's look at the following example:

```
// Out: a list of edges with integer weights
field Out: [{vid, int}]

for u in V
  let minVal = minimum [ e.val | e <- Out[u], e.val > 0 ]
  Result[u] := (minVal != inf ? minVal : -1)
end
```

A list comprehension in Palgol contains the following components:

* A _single_ generator (`e <- Out[u]`) that iterates over an existing list (`Out[u]`) and binds the elements to a variable
* Several filters (`e.val > 0`) that filter out the elements that do not satisfy the condition.
* An expression that will be appended to the generated list (`e.val`).
* A function that reduces the list to a single value (optional).

In this example, Out[u] is an array for storing the outgoing edges.
Then, each vertex picks the minimum positive edge weight among its edge list (using the build-in function **minimum**), then stores the result to the field "Result".
If such edge doesn't exist, -1 should be assigned to Result[u].

Having such list comprehension syntax in mind, next we will show how it can easily describe data communications between vertices.

#### 1.7.1 Neighborhood Field Access

The list comprehension syntax introduced in Palgol is primarily for representing an important remote access pattern -- the neighborhood field access, where every vertex fetches some data from _all_ its neighboring vertices.
Recall that each edge contains a reference and an optional edge weight, then we can fetch the data on a neighboring vertex via the reference stored on that edge.
Here is a more practical example that implements a one-step pagerank algorithm:

```
// In, Out: the lists of incoming/outgoing edges
field In:  [{vid}]
field Out: [{vid}]
// PageRank: the current pagerank for each vertex
field PageRank: float

for u in V
  let t = sum [ PageRank[e.ref] / (to_float Out[e.ref].size()) | e <- In[u] ]
  PageRank[u] := 0.15 / (to_float |V|) + 0.85 * t
end
```

In this example, suppose every vertex has a field "PageRank" for storing the current pagerank value.
Then, this program will do the following things:

* It fetches the normalized pagerank from all incoming neighbors (`e <- In[u]`);
* The normalized pagerank is calculated by using neighbors' local states:
    * `PageRank[e.ref]` is a neighbor's pagerank in the previous superstep;
    * `Out[e.ref].size()` is the number of outgoing edges a neighbor has;
    * `PageRank[e.ref] / (to_float Out[e.ref].size())` is the normalized pagerank, in which the function `to_float` is to make the type correct.
* After collecting the results from neighboring vertices, we calculate the sum of them and bind the result to the variable _t_;
* Calculate the new pagerank by `0.15 / (to_float |V|) + 0.85 * t` where `|V|` stands for the size of the graph.

More formally, a neighborhood field access is a chain access expression `A[B[..X[xx.ref]]]` inside a loop, where _xx_ is a variable iterating over an existing list of edges (e.g., _e_ in `e <- In[u]`), and "A" .. "X" are arbitrary field names.
In this example, `PageRank[e.ref]` and `Out[e.ref]` are recognized as neighborhood field access.

The pagerank program above (which contains a single step) is not completed yet, and the whole implementation can be found in section 1.10, which uses the iteration construct of Palgol to repeatedly run the Palgol step defined above.

#### 1.7.2 Build-in Functions

In Palgol, we provide several build-in functions that can reduce a list to a single element.
In the example above, we use the function `minimum`, which takes a list and returns the minimum element, or **inf** if the list is empty.

Currently, we provide the following six build-in functions to reduce a list to a single value:

* minimum: return the minimum element of the list (or **inf** for empty list)
* maximum: return the maximum element of the list (or -**inf** for empty list)
* sum: the sum of all the elements in the list (elements should be **int** or **float**)
* and: logical _and_ over all the elements in the list
* or: logical _or_ over all the elements in the list
* random: pick a random element in the list, according to the given probability.

The random function of Palgol is somehow special, and let's see how it works:

```
for u in V
  // random :: Num b => [(a, b)] -> (a, b) 
  // the following expression randomly pick a value from [100, 200, 300]
  // with specified weight [1, 2, 5], so the probabilities are:
  //   100 => 1 / (1 + 2 + 5) = 12.5%
  //   200 => 2 / (1 + 2 + 5) = 25%
  //   300 => 5 / (1 + 2 + 5) = 62.5%
  let x: (int, int) = random [ (100, 1), (200, 2), (300, 5) ]
  Picked[u] := fst x   // get the picked integer value
end
```

#### 1.7.3 Special Fields Indicating Edge Lists

Syntactically, Palgol has three pre-defined fields indicating the edge lists.
For directed graphs, **In** and **Out** represents the incoming and outgoing edge lists, while for undirected graphs, **Nbr** is the neighbor list.
Essentially, these are normal fields of a predefined type for representing edges, and most importantly, the compiler assumes a form of symmetry on these fields (namely that every edge is stored consistently on both of its end vertices), and uses the symmetry to produce more efficient code.

Here is a simple example of the list comprehension expression:

```
let minVal = minimum [ F[e.ref] | e <- Out[u], Active[e.ref] ]
```

In this example, every vertex tries to fetch all its active outgoing neighbors' F field, takes the minimum, and stores the final result to the variable _minVal_.
Since we are requiring the data on the outgoing neighbors, we need to consider how to use the message passing to implement such neighborhood data access.
A trivial but inefficient way is to sending a request to each neighbor, and in the consequenting superstep every vertex replies to the requests, but by using the symmetry property, we can say that it is equivelant to let every active vertex to send its F field to all its neighbors, therefore we reduce one round of communication.

In Palgol, we have a strong check to make sure that the **In** (or **Out** and **Nbr**) field exists, so that such optimization is always applicable.
Even the user does not provide such field, we can generate it at the beginning of the programm.
However, since all the fields are mutable in Palgol, users should be extremely careful when trying to modify a neighboring list.

### 1.8 For Loops

For loops is another way to describe loops in Palgol.
Similar to the list comprehension, a for loop always iterates over an existing list, but inside the loop body, we can write all kinds of statements.
The following example shows how to implement the one-step pagerank using the for loop:

```
for u in V
  let mut t = 0.0
  for (e <- In[u])
    t += PageRank[e.ref] / (to_float Out[e.ref].size())
  PageRank[u] := 0.15 / (to_float |V|) + 0.85 * t
end
```

#### 1.8.1 Nested For Loop

_For loop_ can be nested, which gives users more freedom to describe the computation.
However, in this case, we only allow field access from the reference of the outermost loop variable:

```
for u in V
  Result[u] := 0
  for (x <- List1[u])
    for (y <- List2[u])
      // this program doesn't compile ..
      Result[u] += Value[x.ref] * Value[y.ref]
end
```

In this example, we have a nested loop and two remote access patterns `Value[x.ref]` and `Value[y.ref]`.
However, `Value[y.ref]` cannot be handled by Palgol, since _y_ is not the loop variable in the outermost loop.
Such restriction is mainly due to the difficulties in analyzing the data dependency of the program and deciding the most efficient way of implementation.
Briefly speaking, current Palgol cannot handle complex _join_ patterns that fetch data from different lists of sources.

For this example, however, users can make this _join_ operation performed on two local lists:

```
for u in V
  let l1 = [ Value[e.ref] | e <- List1[u] ]
  let l2 = [ Value[e.ref] | e <- List2[u] ]
  Result[u] := 0
  for (x <- l1)
    for (y <- l2)
      Result[u] += x * y
end
``` 

#### 1.8.2 For Loop vs List Comprehension

Here is a little more comments about the choice between for loop and list comprehension.
The design of list comprehension takes the performance seriously into consideration.
When the neighborhood field access is detected in the list comprehension, the compiler will generate the message passing code.
Then, the filters can potentially reduce the messages emitted from the sender, and the functions like `sum`, `minimum` can take advantage of Pregel's combiner interface and further reduce the number of messages.
The combination of all these features makes the generated Pregel code very efficient.

In general, it is reasonable to always try the list comprehension first when you want to use the loop structure.
However, some exceptions exist:

* Nested loop is necessary
* Side effects are needed (e.g., modify the vertex's state or variables)

### 1.9 If

Palgol's **if** is similar to the **if** in a traditional programming language like C.
It is a statement that takes an expression as condition, and goes to one of the two branches according to the evaluation result of the condition.
Here is an example:

```
for u in V
  if (Degree[u] > 0)
    Sgn[u] := 1
  else
    Sgn[u] := 0
end
```

Like how if statement is defined in many languages, the **else** branch can be omitted if nothing should be done.
The following program is equivalent to the one above:

```
for u in V
  Sgn[u] := 0
  if (Degree[u] > 0)
    Sgn[u] := 1
end
```

In Palgol, there is another version of **if**, which takes an expression as condition, and evaluates one of the two expressions accordingly.
Here is another example:

```
for u in V
  Sgn[u] := (Degree[u] > 0 ? 1 : 0)
end
```

The syntax of such expression is `exp0 ? exp1 : exp2`, which is the same as C or Java.
The boolean expression `exp0` is the condition, and `exp1` and `exp2` should have the same type.
In such **if** expression, neither of the two branches can be omitted.

### 1.10 Iterative Computing

Yet, all the examples presented in this tutorial consist of a single Palgol step, but in practice, most vertex-centric algorithms are iterative, and many of them consist of multiple computation stages.
In this part, we are going to see how to construct iterative programs in Palgol.

#### 1.10.1 Repeat for a Fixed Number of Times

Recall that we have presented a one-step pagerank algorithm (in section 1.7.1) to explain the neighborhood field access, where the program body is as follows:

```
for u in V
  let t = sum [ PageRank[e.ref] / (to_float
          Out[e.ref].size()) | e <- In[u] ]
  PageRank[u] := 0.15 / (to_float |V|) + 0.85 * t
end
```

A typical implementation will repeat this procedure for a fixed number of times (e.g., 30 times), assuming the final result converges well.
It is usually accomplished by using the global step counter (an interface of Pregel), and every vertex votes to halt when the step number exceeds a certain number, so that the body of the iteration runs exactly 30 times.

In Palgol, we do such thing in a totally different way.
We don't have a global step counter, but we just need to enclose the iteration body (which is a single Palgol step) with the following code:

```
iter c in range(0, 30)
  for u in V
    let t = sum [ PageRank[e.ref] / (to_float Out[e.ref].size())
            | e <- In[u] ]
    PageRank[u] := 0.15 / (to_float |V|) + 0.85 * t
  end
end
```

In this example, `range(0, 30)` represents an integer list containing 0..29, then we bind these values to the variable _c_ one by one from the smallest to the largest, and run the Palgol step inside it (line 2-6).
No matter whether _c_ is inside the loop body, it will be executed exactly 30 times.

Repeating a Palgol step for a fixed number of times is done exactly in this way.
Besides, what can be put inside `iter .. end` is not limited to a single Palgol step, but can be also any Palgol program, like multiple Palgol steps, or even another iteration.

For pagerank algorithm, there still needs an initialization step before the iterative computation we presented above.
For completeness, we just put the whole program here:

```
input[Id, In, Out]
output[Id, PageRank]

for u in V
  local PR[u] := 1.0 / (to_float |V|)
end
iter c in range(0, 30)
  for u in V
    let t = sum [ PageRank[e.ref] / (to_float Out[e.ref].size())
            | e <- In[u] ]
    PageRank[u] := 0.15 / (to_float |V|) + 0.85 * t
  end
end
```

#### 1.10.2 Find the Fixed-point

There is another very useful type of iteration used in many vertex-centric graph algorithms, which is iterating some computation until one or more fields stabilize (a.k.a. reaching a fixed-point).
Let's see the following example:

```
field Label: int
do
  for u in V
    // here 'a <?= b' means 'a := min(a, b)'
    Label[u] <?= minimum [ Label[e.ref] | e <- In[u] ]
  end
until fix[Label]
```

This algorithm is usually called "label propagation", where every vertex initially holds some label, and tries to propagate it to the whole graph.
To achieve this, in each step, every vertex collects neighbors' current label, and takes the minimum among both its own label and the collected ones.
Unlike the previous example, users cannot tell how many steps it requires to finish the computation.
However, the computation will finally converge, when every vertex holds the label less or equal to all its incoming neighbors.
In such case, the graph will no longer change even we run this Palgol step again.

To achieve this, we only need to enclose the Palgol step with `do .. until` and use `fix[Label]` as termination condition, meaning that the iteration stops when the field "Label" no longer changes on every vertex.
In general, we can put any number of fields inside the bracket, and the iteration stops when _all_ these fields stabilize.

#### 1.10.3 General Termination Conditions

In this part, we are going to introduce two more general termination conditions for describing (do .. until) iteration, which are **forall** and **exists**.
Let's first go through the syntax:

```
for u in V
  Fix[u] := false
do
  for u in V
    let t = minimum [ Label[e.ref]
          | e <- In[u], !Fix[e.ref] ]
    Fix[u] := true
    if (t < Label[u])
      Label[u] := t
      Fix[u] := false
  end
until forall u. Fix[u]
```

This program implements the label propagation algorithm in another way, using **forall** as termination condition after `do .. until`.
The syntax of the forall termination condition is `forall xx. exp` where _xx_ is an arbitrary variable indicating a vertex in graph, and _exp_ is the predicate.
The iteration ends when all vertices satisfy the predicate.

From this example, we can see that "forall" is able to implement the termination condition of finding the fixed-point.
The idea is simply maintaining a boolean field on each vertex indicating whether each vertex stabilizes in the previous step, and using "forall" to collect every vertex's result and get the final decision.
In addition, the boolean field itself can help to optimize the program, like controlling whether to fetch data from other vertices (e.g., `!Fix[e.ref]`).
Anyway, it is just a general termination condition to iterate some computation.

Finally, we have the **exists** termination condition: `exists xx. exp` which is also appended to the `do .. until`, indicating that the iteration terminates when there exists any vertex satisfy the termination condition.

### 1.11 Remote Writes

In Palgol, modification of the current vertex's state is achieved by using an assignment like this:

```
for u in V
  Active[u] := Degree[u] > 0
  Sum[u] += sum [ X[e.ref] | e <- In[u] ]
end
```

It is natural to think whether we can directly modify the state of other vertices.
The answer is yes.
In Palgol, we have _remote write_ which is exactly for this purpose.
Let's first see the syntax of remote write:

```
for u in V
  remote Active[Ref[u]] |= Degree[Ref[u]] > 0
  for (e <- Out[u])
    remote Sum[e.ref] += X[u]
end
```

First, the remote writes always start with the keyword **remote**, follows which is an assignment-like statement.
Unlike local writes that require the lvalue to be a local field (e.g., `Active[u]`), in remote writes you can modify any vertex you want.
For example, writing `Active[Ref[u]]` is to modify the "Active" field of the vertex Ref[u], while writing `Sum[e.ref]` is to modify the "Sum" field of the vertex `e.ref`.

Although local writes and remote writes are syntactically similar, their semantics are actually quite different.
Moreover, remote writes further impose some other restrictions, which is to help users correctly handle some well-known problems in distributed computing.
Next, we are going to make the difference clear to users.

#### 1.11.1 Reduce Operator

The first important thing about remote write is that, when naively allowing vertices to modify other vertices' states, non-deterministic behaviors happen.
The reason is that, when a vertex tries to write to another vertex, there also might be some other vertex trying to modify the same vertex.
In this case, which modification happens first should be seriously considered.
For example, if you use assignment in remote write statement:

```
for u in V
  remote Active[Ref[u]] := Degree[u] > 0
```

Then, it is possible that a vertex is modified by many vertices with different values, then the last modification will produces the final result.
Such non-deterministic behavior could make users hard to reason about the program.
Indeed, Palgol will produce such warning message when you try to do such kind of things:

```
line 2.25 warning: risky to use ':=' to update non-local field
  remote Active[Ref[u]] := Degree[u] > 0
                        ^
```

A simple solution to this problem is that, we only allow users to use _reduce operators_ to specify the remote writes, which naturally avoids the non-determinism in high-level specification.
Essentially, remote write in Palgol is equivalent to a [fold](https://en.wikipedia.org/wiki/Fold_(higher-order_function)) in functional programming languages:
the input is an initial value, a list of remote updates and a binary function, while the output is generated by taking each element out of the list, and update the initial value by using the binary function.
By carefully selecting the binary function, the non-deterministic behaviors can be avoided.

Current Palgol supports the following operators for remote writes:

```
+= -= *= /= &= |= ^= <?= >?= .append(..)
```

Among these operators, `<?=` and `>?=` are special ones that mean `min` and `max` respectively.
For example, `a <?= b` is equivalent to `a := min(a, b)`, and we design this operator just for conciseness.
Then, the operator `.append(..)` is designed for array, which allows you append elements to a remote list.

#### 1.11.2 Deferred Assignments

Another important thing about remote writes is that, they are deferred assignments that are executed by a different mechanism (in particular, the message passing) compared with local assignments.
To better understand this, let's see the following example:

```
for u in V
  X[u] := 5
  remote X[u] += 3
  X[u] /= 2
end
```

The "X" field of every vertex becomes 5 after execution, instead of 4 (where 4 is calculated by considering the second assignment as a local one).
The reason is that, the assignments prefixed with **remote** actually happen after *all* local assignments are executed in this step.

Let's see what exactly happens here.
First, `X[u] := 5` is a normal local assignment that assign 5 to every vertex's "X" field.
Then, `remote X[u] += 3` can be seen as sending a message to itself with an integer 3, but the message will be handled in the future.
Next, `X[u] /= 2` is a local assignment that divides the current X[u] by two.
Since current X[u] is 5, this assignment will make "X" field to be 2.

Finally, when all the code in this Palgol step is executed, we add an additional superstep to handle those remote write messages sent from the previous steps.
Here, every vertex will receive a single message containing 3, then it uses `+=` to add it to "X" field, making the final result to be 5.

The following pseudo code better illustrates what happened here:

```
for u in V
  X[u] := 5
  send 3 to vertex u
  X[u] /= 2  // X[u] == 2
end
// messages are handled by the system
// an additional step for handling remote writes
for u in V
  let msgs = get_messages(u)  // msgs = [3]
  for (elem <- msgs)
    X[u] += elem  // '+=' happens here
end
```

#### 1.11.3 Conflict Updates

The last thing we are going to discuss is the conflict updates, which brings another restriction on remote writes.
Let's see the following program:

```
for u in V
  if (Test[u])
    remote Sum[Ref[u]] += X[u]
  else
    remote Sum[Ref[u]] -= X[u]  // compiling error
end
```

The compiler will produce the following error message:

```
line 5.24 error: update remote field with different operators
    remote Sum[Ref[u]] -= X[u]  // compiling error
                       ^
```

It tells you that you use different operators (`+=` and `-=`) in the same Palgol step on the same remote field, which may cause non-deterministic behavior in execution.
We call it _conflict updates_.
Palgol rejects the programs containing conflict updates.
For the example above, the following program is definitely better:

```
for u in V
  if (Test[u])
    remote Sum[Ref[u]] += X[u]
  else
    remote Sum[Ref[u]] += -X[u]
end
```

or make it like this:

```
for u in V
  remote Sum[Ref[u]] += (Test[u] ? X[u] : -X[u])
end
```

### 1.12 Aggregator

Aggregator is a standard mechanism provided by every Pregel system for global communication, like collecting the sum of a field among a subset of the vertices, or make a consesus by combining the decision of each vertex.
In Palgol, we provide a special syntax for aggregator.
As usual, we start from an example:

```
for u in V
  let agg s = sum [ PageRank[v] | v <- V, Out[v].size() == 0 ]
  let t = sum [ PageRank[e.ref] / (to_float Out[e.ref].size())
        | e <- In[u] ]
  PageRank[u] := 0.15 / (to_float |V|) + 0.85 * (t + s)
end
```

This step implements another version of PageRank by adding additonal links from the sink pages (those web pages without outgoing links) to a virtual page to avoid unexpected accumulation of PageRank.
In this algorithm, the system first calculates the sum of the PageRank of all sink vertices, then redistributes it to all vertices in the graph.
The collecting procedure is exactly implemented by an aggregator.
In Palgol, an aggregator starts from **let agg** followed by a variable name and the description of the aggregator, including the aggregation operator, the expression provided by each vertex, a generator (always in the form of "var <- **V**") and several filters (if needed).


### 1.13 Foreign Function Interface (FFI)

Palgol is a domain-specific language with a high-level programming model for describing vertex-centric graph algorithm, and the main design concern is to make the compiler easy to manage the data dependency and translate the remote access to the message passing interface of Pregel.
The language itself is however much simpler than a general programming language.
To make Palgol more practical, a natural idea is to introduce the foreign function interface (FFI) to give users more freedom to describe the _local_ computation they want.

In Palgol, users only need to declare the name and type of the function, then they can use this function in the program, and implement the function somewhere else.
Let's look at this example:

```
input[Id, Out, X, Y]
output[Id, Result]

extern make_unique([{vid, int}], [{vid, int}]) -> [{vid, int}]

for u in V
  let l1 = [ {e.ref, X[e.ref]} | e <- In[u], e.ref > u ]
  let l2 = [ {e.ref, Y[e.ref]} | e <- In[u], e.ref < u ]
  Result[u] := make_unique(l1, l2)
end
```

The keyword **extern** is used to declare the external function, followed by the name of the function, the type of each argument, and the return type.
The arguments are enclosed by parenthesis and separated by comma, and the return type is indicated by the arrow after the argument list.

Sometimes, the type signature of the arguments or return type can be very complex, so we may need to use type alias (section 1.5.7) to make the declaration more beautiful:

```
type myList = [{vid, int}]
extern make_unique(myList, myList) -> myList
```

Palgol currently compiles to [Pregel+](http://www.cse.cuhk.edu.hk/pregelplus/), an open-sourced Pregel system written in C++.
A Palgol program using external function will compile to a C++ program containing a header "external.hpp",
and users should implement the external functions in this file.
For example, the "external.hpp" may look like this:

```
#include <vector>

using namespace std;

// myList maps to [{vid, int}] in Palgol
typedef vector<pair<int, int> > myList;

// function name should be the same
// function type only need to be compatible (const, & .. are optional)
myList make_unique(const myList &l1, const myList &l2) {
	myList ret;
	ret.insert(ret.end(), l1.begin(), l1.end());
	ret.insert(ret.end(), l2.begin(), l2.end());
	sort(ret.begin(), ret.end());
	ret.erase(unique(ret.begin(), ret.end()), ret.end());
	return ret;
}
```

There do have some rules to map a type in Palgol to a corresponding one in C++:

* **int**, **float**, **bool** in Palgol are translated to **int**, **double**, **bool** in C++
* **vid** type currently compiles to **int** in C++
* pair type compiles to `std::pair`
* list type compiles to `std::vector`
* edge type {**vid**} compiles to **int**
* edge type {**vid**, _} compiles to `std::pair<int, ?>` depending on the concrete type of the second component

These rules are quite intuitive.
Furthermore, our compiler provides an option to output the type signatures for those external functions.
Please type `./palgol` for more detail.
