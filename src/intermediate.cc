#include <cstdio>
#include <cstdlib>

#include "ast.h"
#include "codegen.h"
#include "config.h"
#include "errorinfo.h"
#include "intermediate.h"
#include "parser.h"
#include "types.h"

using namespace std;

static char buf[128];

static void pretty_float(char *s, double v) {
	const double eps = 1e-8;
	int x, f = (v < -eps ? -1 : v > eps);
	char *c = buf + 31;
	if (v < 0) v = -v;
	v += eps;
	x = (int) v;
	v -= x;
	*c = 0;
	do {
		*--c = '0' + (x % 10);
		x /= 10;
	} while (x > 0);
	if (f == -1) *s++ = '-';
	while (*c) *s++ = *c++;
	double thr = 1e-7;
	*s++ = '.';
	for (int i = 0; i < 6; i++) {
		v *= 10;
		x = (int) v;
		v -= x;
		*s++ = '0' + x;
		thr *= 10;
		if (v < thr) break;
	}
	*s = 0;
}

static const char *getCppStr(int op) {
	if (op == tok_mass)
		return "=";
	else
		return getTokenStr(op);
}

CombinerT op2combiner(int op) {
	if (op == tok_mmax) return CombinerT::c_max;
	if (op == tok_mmin) return CombinerT::c_min;
	if (op == tok_madd) return CombinerT::c_sum;
	if (op == tok_mand) return CombinerT::c_and;
	if (op == tok_mor ) return CombinerT::c_or ;
	if (op == tok_mrand) return CombinerT::c_rand;
	if (op == tok_marbi) return CombinerT::c_arbi;
	return CombinerT::c_null;
}

string FieldIR::getName() const {
	return field_->getName();
}

string AggFieldIR::getName() const {
	return field_->getName();
}

void StmtListIR::add(StmtIR *stmt) {
	list_.push_back(stmt);
}

StmtIR *StmtListIR::get(int index) {
	return list_[index];
}

int StmtListIR::size() const {
	return list_.size();
}

void SendMsgIR::dumpTypes(FILE *f) const {
	for (int i = 0; i < list_.size(); i++) {
		if (i > 0)
			fprintf(f, " and ");
		if (list_[i]->type() != nullptr)
			list_[i]->type()->print(f);
		else
			fprintf(f, "unknown type");
	}
	fprintf(f, "\n");
}

vector<BasicType*> SendMsgIR::flatten() {
	vector<BasicType*> types;
	for (int i = 0; i < list_.size(); i++)
		msgTypes_.push_back(list_[i]->type()->flatten(types));
	return types;
}

RemoteData::RemoteData(int ch, Identifier *cur, std::string lvar) :
		cur_(cur), ch_(ch), lvar_(lvar) {
	if (cur->type()->type() != PalgolTypes::t_edge)
		prtError(*cur, "use record type \"{vid, ?}\" to enable remote access");
	et_ = (EdgeType*) cur->type();
	name_ = rename(cur->getName(), getIREnv()->getPID());
}

ReadMsgIR *RemoteData::add(ExprIR *expr) {
	for (int i = 0; i < list_.size(); i++)
		if (expr->equals(list_[i]))
			return new ReadMsgIR(expr->type(), name_, ch_, i);
	int index = list_.size();
	list_.push_back(expr);
	return new ReadMsgIR(expr->type(), name_, ch_, index);
}

NbrGetData::NbrGetData(int ch, Identifier *cur, FieldName *gen) :
		RemoteData(ch, cur, rename("e", getIREnv()->getPID())), gen_(gen) {
}

void NbrGetData::addPredicate(ExprIR *expr, bool rmt_lp) {
	if (rmt_lp)
		in_.add(expr);
	else
		out_.add(expr);
}

ExprIR *NbrGetData::getFst() {
	return new SelectIR(et_->getFirst(), getOper(tok_fst),
			new VarIR(et_, lvar_));
}

ExprIR *NbrGetData::getSnd() {
	return new SelectIR(et_->getSecond(), getOper(tok_snd),
			new VarIR(et_, lvar_));
}

StmtIR *NbrGetData::produce(IREnv *env) {
	FieldIR *gen = new FieldIR(new ListType(et_), gen_);
	VarIR *var = new VarIR(et_, lvar_);
	ExprIR *dest = new SelectIR(getVidType(), getOper(tok_fst), var);
	SendMsgIR *sm = new SendMsgIR(ch_, list_, dest, env->getCombiner());
	StmtIR *stmt = in_.produce(sm);
	env->getMM()->addSM(sm);
	return out_.produce(new ForStmtIR(lvar_, gen, stmt));
}

ReqResData::ReqResData(int ch, Identifier *cur, FieldName *fn) :
		RemoteData(ch, cur, "msg"), fn_(fn), ew_(0) {
	req_ch_ = getIREnv()->newChannel();
}

void ReqResData::addReqPredicate(ExprIR *expr) {
	req_.add(expr);
}

void ReqResData::addResPredicate(ExprIR *expr, bool rmt_lp) {
	if (rmt_lp)
		res_in_.add(expr);
	else
		res_out_.add(expr);
}

ExprIR *ReqResData::getFst() {
	return new ReadMsgIR(et_->getFirst(), lvar_, req_ch_, 0);
}

ExprIR *ReqResData::getSnd() {
	ew_ = 1; // send edge weight in request
	return new ReadMsgIR(et_->getSecond(), lvar_, req_ch_, 1);
}

StmtIR *ReqResData::produce(IREnv *env) {
	// requesting vertex's code
	FieldIR *fi = new FieldIR(new ListType(cur_->type()), fn_);
	VarIR *var = new VarIR(et_, name_);
	ExprIR *dst = new SelectIR(et_->getFirst(), getOper(tok_fst), var);
	vector<ExprIR*> vec{dst};
	if (ew_) {
		ExprIR *w = new SelectIR(et_->getSecond(), getOper(tok_fst), var);
		vec.push_back(w);
	}
	SendMsgIR *sm1 = new SendMsgIR(req_ch_, vec, dst, CombinerT::c_null);
	ForStmtIR *req = new ForStmtIR(name_, fi, req_.produce(sm1));
	env->getMM()->addSM(sm1);
	// responsing vertex's code
	SendMsgIR *sm2 = new SendMsgIR(ch_, list_, getFst(), CombinerT::c_null);
	ScanStmtIR *scan = new ScanStmtIR(req_ch_, lvar_, req, res_in_.produce(sm2));
	env->getMM()->addSM(sm2);
	return res_out_.produce(scan);
}

int SendPatt::depth() {
	return max(cont_->depth(), dest_->depth()) + 1;
}

/* implement the interface in class ExprIR
 *   ExprIR *genSteps(IREnv *env, int step);
 * take the stmts out of ExprStmtIR; add stmts to env;
 */
ExprIR *VarIR::genSteps(IREnv *env, int step) {
	env->useVarInStep(this, step);
	return this;
}

ExprIR *PairIR::genSteps(IREnv *env, int step) {
	fst_ = fst_->genSteps(env, step);
	snd_ = snd_->genSteps(env, step);
	return this;
}

ExprIR *IfExprIR::genSteps(IREnv *env, int step) {
	cond_ = cond_->genSteps(env, step);
	e1_ = e1_->genSteps(env, step);
	e2_ = e2_->genSteps(env, step);
	return this;
}

ExprIR *BinaryIR::genSteps(IREnv *env, int step) {
	e1_ = e1_->genSteps(env, step);
	e2_ = e2_->genSteps(env, step);
	return this;
}

ExprIR *UnaryIR::genSteps(IREnv *env, int step) {
	expr_ = expr_->genSteps(env, step);
	return this;
}

ExprIR *ExprStmtIR::genSteps(IREnv *env, int step) {
	stmts_->genSteps(env, step);
	if (expr_->itype() == IRType::ir_var)
		env->useVarInStep((VarIR*) expr_, step);
	return expr_;
}

/* implement the interface in class StmtIR
 *   void genSteps(IREnv *env, int step);
 */
void StmtIR::genSteps(IREnv *env, int step) {
	env->addToStep(step, this);
}

void StmtListIR::genSteps(IREnv *env, int step) {
	for (auto stmt : list_)
		stmt->genSteps(env, step);
}

void AssignIR::genSteps(IREnv *env, int step) {
	if (lval_->itype() == IRType::ir_var)
		env->useVarInStep((VarIR*) lval_, step);
	expr_ = expr_->genSteps(env, step);
	if (!lval_->equals(expr_))
		env->addToStep(step, this);
}

void RemoteIR::genSteps(IREnv *env, int step) {
	int ch = env->remoteUpdate(fn_, expr_->type(), op_);
	expr_ = expr_->genSteps(env, step);
	dest_ = dest_->genSteps(env, step);
	SendMsgIR *sm = new SendMsgIR(ch, vector<ExprIR*>(1, expr_),
			dest_, op2combiner(op_->type));
	env->getMM()->addSM(sm);
	env->addToStep(step, sm);
}

void ForStmtIR::genSteps(IREnv *env, int step) {
	expr_ = expr_->genSteps(env, step);
	env->pushStmts(step);
	stmt_->genSteps(env, step);
	StmtListIR *stmts = env->popStmts(step);
	env->addToStep(step, new ForStmtIR(name_, expr_, stmts));
}

void ScanStmtIR::genSteps(IREnv *env, int step) {
	if (sender_ != nullptr)
		sender_->genSteps(env, step - 1);
	env->pushStmts(step);
	stmt_->genSteps(env, step);
	StmtListIR *stmts = env->popStmts(step);
	env->addToStep(step, new ScanStmtIR(ch_, name_, nullptr, stmts));
	env->addChannel(step, ch_);
}

void IfStmtIR::genSteps(IREnv *env, int step) {
	StmtListIR *s1 = nullptr, *s2 = nullptr;
	ExprIR *cond = cond_->genSteps(env, step);
	env->pushStmts(step);
	s1_->genSteps(env, step);
	s1 = env->popStmts(step);
	if (s2_ != nullptr) {
		env->pushStmts(step);
		s2_->genSteps(env, step);
		s2 = env->popStmts(step);
	}
	env->addToStep(step, new IfStmtIR(cond, s1, s2));
}

void SendMsgIR::genSteps(IREnv *env, int step) {
	for (int i = 0; i < list_.size(); i++)
		list_[i] = list_[i]->genSteps(env, step);
	dest_ = dest_->genSteps(env, step);
	env->addToStep(step, this);
}

/* implement the interface in class IR:
 * 	 void print(FILE *f);
 */
void IntIR::print(FILE *f) const {
	if (spl_ == 0)
		fprintf(f, "(Int %d)", val_);
	if (spl_ == 1)
		fprintf(f, "(Int inf)");
	if (spl_ == -1)
		fprintf(f, "(Int -inf)");
}

void FloatIR::print(FILE *f) const {
	if (spl_ == 0)
		fprintf(f, "(Float %.2f)", val_);
	if (spl_ == 1)
		fprintf(f, "(Float inf)");
	if (spl_ == -1)
		fprintf(f, "(Float -inf)");
}

void BoolIR::print(FILE *f) const {
	fprintf(f, "(Bool %s)", (val_ ? "true" : "false"));
}

void EmptyListIR::print(FILE *f) const {
	fprintf(f, "(EmptyList)");
}

void GraphSizeIR::print(FILE *f) const {
	fprintf(f, "(GraphSize)");
}

void PairIR::print(FILE *f) const {
	fprintf(f, "(Pair ");
	fst_->print(f);
	fprintf(f, " ");
	snd_->print(f);
	fprintf(f, ")");
}

void IfExprIR::print(FILE *f) const {
	fprintf(f, "(IfExpr ");
	cond_->print(f);
	fprintf(f, " ");
	e1_->print(f);
	fprintf(f, " ");
	e2_->print(f);
	fprintf(f, ")");
}

void IDIR::print(FILE *f) const {
	fprintf(f, "(ID)");
}

void FieldIR::print(FILE *f) const {
	fprintf(f, "(FieldIR %s)", field_->getName().c_str());
}

void AggFieldIR::print(FILE *f) const {
	fprintf(f, "(AggFieldIR %s)", field_->getName().c_str());
}

void VarIR::print(FILE *f) const {
	fprintf(f, "(VarIR %s)", name_.c_str());
}

void SendPatt::print(FILE *f) const {
	fprintf(f, "(SendPatt ");
	cont_->print(f);
	fprintf(f," ");
	dest_->print(f);
	fprintf(f, ")");
}

void BinaryIR::print(FILE *f) const {
	fprintf(f, "(Binary ");
	e1_->print(f);
	fprintf(f, " \"%s\" ", getTokenStr(Token(0, 0, op_->type)));
	e2_->print(f);
	fprintf(f, ")");
}

void FunctionIR::print(FILE *f) const {
	fprintf(f, "(Function \"%s\"", name_.c_str());
	for (int i = 0; i < args_.size(); i++) {
		fprintf(f, " ");
		args_[i]->print(f);
	}
	fprintf(f, ")");
}

void NotIR::print(FILE *f) const {
	fprintf(f, "(Not ");
	expr_->print(f);
	fprintf(f, ")");
}

void NegativeIR::print(FILE *f) const {
	fprintf(f, "(Negative ");
	expr_->print(f);
	fprintf(f, ")");
}

void SelectIR::print(FILE *f) const {
	fprintf(f, "(Select \"%s\" ",
		(op_->type == tok_fst ? "fst" : "snd"));
	expr_->print(f);
	fprintf(f, ")");
}

void SizeIR::print(FILE *f) const {
	fprintf(f, "(Size ");
	expr_->print(f);
	fprintf(f, ")");
}

void ToiIR::print(FILE *f) const {
	fprintf(f, "(ToInt ");
	expr_->print(f);
	fprintf(f, ")");
}

void TofIR::print(FILE *f) const {
	fprintf(f, "(ToFloat ");
	expr_->print(f);
	fprintf(f, ")");
}

void ReadMsgIR::print(FILE *f) const {
	fprintf(f, "(ReadMsg %d %d)", ch_, index_);
}

void AggIR::print(FILE *f) const {
	fprintf(f, "(Aggregator \"%s\")", name_.c_str());
}

void ReqResp::print(FILE *f) const {
	fprintf(f, "(ReqResp \"%s[%s[_]]\")",
			val_->getName().c_str(),
			dst_->getName().c_str());
}

void ExprStmtIR::print(FILE *f) const {
	fprintf(f, "(ExprStmt ");
	expr_->print(f);
	fprintf(f, " ");
	stmts_->print(f);
	fprintf(f, ")");
}

void StmtListIR::print(FILE *f) const {
	fprintf(f, "(StmtList");
	for (int i = 0; i < list_.size(); i++) {
		fprintf(f, " ");
		list_[i]->print(f);
	}
	fprintf(f, ")");
}

void IfStmtIR::print(FILE *f) const {
	fprintf(f, "(IfStmt ");
	cond_->print(f);
	fprintf(f, " ");
	s1_->print(f);
	if (s2_ != nullptr) {
		fprintf(f, " ");
		s2_->print(f);
	}
	fprintf(f, ")");
}

void SendMsgIR::print(FILE *f) const {
	fprintf(f, "(SendMsg C%d ", ch_);
	dest_->print(f);
	fprintf(f, " (List");
	for (int i = 0; i < list_.size(); i++) {
		fprintf(f, " ");
		list_[i]->print(f);
	}
	fprintf(f, "))");
}

void AssignIR::print(FILE *f) const {
	fprintf(f, "(Assign ");
	lval_->print(f);
	fprintf(f, " \"%s\" ", getTokenStr(op_->type));
	expr_->print(f);
	fprintf(f, ")");
}

void RemoteIR::print(FILE *f) const {
	fprintf(f, "(Remote %s ", fn_->getName().c_str());
	dest_->print(f);
	fprintf(f, " \"%s\" ", getTokenStr(op_->type));
	expr_->print(f);
	fprintf(f, ")");
}

void ForStmtIR::print(FILE *f) const {
	fprintf(f, "(ForStmt %s ", name_.c_str());
	expr_->print(f);
	fprintf(f, " ");
	stmt_->print(f);
	fprintf(f, ")");
}

void ProcedureIR::print(FILE *f) const {
	func_->print(f);
}

void GetMsgIR::print(FILE *f) const {
	fprintf(f, "(GetMsg ");
	var_->print(f);
	fprintf(f, " C%d)", ch_);
}

void ScanStmtIR::print(FILE *f) const {
	fprintf(f, "(ScanMsg C%d ", ch_);
	if (sender_ != nullptr) {
		sender_->print(f);
		fprintf(f, " ");
	}
	stmt_->print(f);
	fprintf(f, ")");
}

void VoteToHalt::print(FILE *f) const {
	fprintf(f, "(VoteToHalt)");
}

void ReturnIR::print(FILE *f) const {
	fprintf(f, "(Return)");
}

/* Implement the interface for class IR:
 *   bool equals(const ExprIR *expr);
 */
bool ExprIR::equals(const ExprIR *other) const {
	return other->itype() == itype_;
}

bool IntIR::equals(const ExprIR *other) const {
	if (other->itype() != itype_) return false;
	IntIR *it = (IntIR*) other;
	return (val_ == it->val_ && spl_ == it->spl_);
}

bool FloatIR::equals(const ExprIR *other) const {
	if (other->itype() != itype_) return false;
	FloatIR *it = (FloatIR*) other;
	return (val_ == it->val_ && spl_ == it->spl_);
}

bool BoolIR::equals(const ExprIR *other) const {
	if (other->itype() != itype_) return false;
	return (val_ == ((BoolIR*) other)->val_);
}

bool PairIR::equals(const ExprIR* other) const {
	if (other->itype() != itype_) return false;
	PairIR *it = (PairIR*) other;
	return fst_->equals(it->fst_) && snd_->equals(snd_);
}

bool IfExprIR::equals(const ExprIR *other) const {
	if (other->itype() != itype_) return false;
	IfExprIR *it = (IfExprIR*) other;
	return cond_->equals(it->cond_) &&
			e1_->equals(it->e1_) && e2_->equals(it->e2_);
}

bool FieldIR::equals(const ExprIR *other) const {
	if (other->itype() != itype_) return false;
	FieldIR *it = (FieldIR*) other;
	return field_->getName() == it->field_->getName();
}

bool AggFieldIR::equals(const ExprIR *other) const {
	if (other->itype() != itype_) return false;
	AggFieldIR *it = (AggFieldIR*) other;
	return field_->getName() == it->field_->getName();
}

bool VarIR::equals(const ExprIR *other) const {
	if (other->itype() != itype_) return false;
	VarIR *it = (VarIR*) other;
	return name_ == it->name_;
}

bool BinaryIR::equals(const ExprIR *other) const {
	if (other->itype() != itype_) return false;
	BinaryIR *it = (BinaryIR*) other;
	return e1_->equals(it->e1_) && e2_->equals(it->e2_);
}

bool UnaryIR::equals(const ExprIR *other) const {
	if (other->itype() != itype_) return false;
	UnaryIR *it = (UnaryIR*) other;
	return expr_->equals(it->expr_);
}

bool AggIR::equals(const ExprIR *other) const {
	if (other->itype() != itype_) return false;
	AggIR *it = (AggIR*) other;
	return name_ == it->name_;
}

bool ReqResp::equals(const ExprIR *other) const {
	if (other->itype() != itype_) return false;
	ReqResp *it = (ReqResp*) other;
	return (val_ == it->val_ && dst_ == it->dst_);
}

/* Implement the interface for class IR:
 *   void calcDepth(IREnv *env);
 */
void VarIR::calcDepth(IREnv *env) {
	env->useVarIR(this);
}

void PairIR::calcDepth(IREnv *env) {
	fst_->calcDepth(env);
	snd_->calcDepth(env);
	depth_ = max(fst_->depth(), snd_->depth());
}

void IfExprIR::calcDepth(IREnv *env) {
	cond_->calcDepth(env);
	e1_->calcDepth(env);
	e2_->calcDepth(env);
	depth_ = max(cond_->depth(), max(e1_->depth(), e2_->depth()));
}

void BinaryIR::calcDepth(IREnv *env) {
	e1_->calcDepth(env);
	e2_->calcDepth(env);
	depth_ = max(e1_->depth(), e2_->depth());
}

void UnaryIR::calcDepth(IREnv *env) {
	expr_->calcDepth(env);
	depth_ = expr_->depth();
}

void StmtListIR::calcDepth(IREnv *env) {
	// TODO not correct! should handle let-stmt seperatedly!
	for (int i = 0; i < list_.size(); i++) {
		list_[i]->calcDepth(env);
		depth_ = max(depth_, list_[i]->depth());
	}
}

void ExprStmtIR::calcDepth(IREnv *env) {
	stmts_->calcDepth(env);
	depth_ = stmts_->depth();
}

void AssignIR::calcDepth(IREnv *env) {
	if (expr_ == nullptr) {
		lval_->print(stderr);
		fprintf(stderr, " %s \n", getTokenStr(op_->type));
		prtError("AssignIR::calcDepth(IREnv *env): expr is nullptr");
	}
	expr_->calcDepth(env);
	depth_ = expr_->depth();
}

void RemoteIR::calcDepth(IREnv *env) {
	dest_->calcDepth(env);
	expr_->calcDepth(env);
	depth_ = max(dest_->depth(), expr_->depth());
}

void ForStmtIR::calcDepth(IREnv *env) {
	stmt_->calcDepth(env);
	depth_ = stmt_->depth();
}

void GetMsgIR::calcDepth(IREnv *env) {
	depth_ = var_->depth();
}

void ScanStmtIR::calcDepth(IREnv *env) {
	sender_->calcDepth(env);
	stmt_->calcDepth(env);
	depth_ = max(sender_->depth() + 1, stmt_->depth());
}

void IfStmtIR::calcDepth(IREnv *env) {
	cond_->calcDepth(env);
	s1_->calcDepth(env);
	depth_ = max(cond_->depth(), s1_->depth());
	if (s2_ != nullptr) {
		s2_->calcDepth(env);
		depth_ = max(depth_, s2_->depth());
	}
}

void SendMsgIR::calcDepth(IREnv *env) {
	for (int i = 0; i < list_.size(); i++) {
		list_[i]->calcDepth(env);
		depth_ = max(depth_, list_[i]->depth());
	}
}

static inline void lparen(Text *text, int op1, int op2) {
	if (getPrec(op1) >= getPrec(op2))
		text->append("(");
}

static inline void rparen(Text *text, int op1, int op2) {
	if (getPrec(op1) >= getPrec(op2))
		text->append(")");
}

/* Implement the interface for class ReadMsgIR, SelectIR
 *   ExprIR *tr_readmsg();
 */
ExprIR *ReadMsgIR::tr_readmsg() {
	MessageManager *mm = getCodeGen()->getMM();
	return mm->readMessage(ch_, index_, name_);
}

ExprIR *SelectIR::tr_readmsg() {
	expr_ = expr_->tr_readmsg();
	if (expr_->itype() == IRType::ir_pair) {
		PairIR *p = (PairIR*) expr_;
		if (op_->type == tok_fst)
			return p->getFirst();
		else
			return p->getSecond();
	}
	return this;
}

/* Implement the interface for class ExprIR, StmtIR
 *   void pregelPlus(Text *text, int op);
 */
void IntIR::pregelPlus(Text *text, int op) const {
	if (spl_ == -1)
		text->append("-INT_MAX");
	else if (spl_ == 1)
		text->append("INT_MAX");
	else {
		sprintf(buf, "%d", val_);
		text->append(string(buf));
	}
}

void FloatIR::pregelPlus(Text *text, int op) const {
	if (spl_ == -1)
		text->append("-DBL_MAX");
	else if (spl_ == 1)
		text->append("DBL_MAX");
	else {
		pretty_float(buf, val_);
		text->append(string(buf));
	}
}

void BoolIR::pregelPlus(Text *text, int op) const {
	text->append(val_ ? "true" : "false");
}

void EmptyListIR::pregelPlus(Text *text, int op) const {
	type_->toCPP(text);
	text->append("()");
}

void GraphSizeIR::pregelPlus(Text *text, int op) const {
	text->append("get_vnum()");
}

void PairIR::pregelPlus(Text *text, int op) const {
	text->append("make_pair(");
	fst_->pregelPlus(text, tok_comma);
	text->append(", ");
	snd_->pregelPlus(text, tok_comma);
	text->append(")");
}

void IfExprIR::pregelPlus(Text *text, int op) const {
	text->append("(");
	cond_->pregelPlus(text, tok_ques);
	text->append(" ? ");
	e1_->pregelPlus(text, tok_colon);
	text->append(" : ");
	e2_->pregelPlus(text, tok_colon);
	text->append(")");
}

void IDIR::pregelPlus(Text *text, int op) const {
	text->append("id");
}

void FieldIR::pregelPlus(Text *text, int op) const {
	text->append("value().");
	text->append(field_->getName());
}

void AggFieldIR::pregelPlus(Text *text, int op) const {
	text->append("v->value().");
	text->append(field_->getName());
}

void VarIR::pregelPlus(Text *text, int op) const {
	IREnv *env = getIREnv();
	if (env->isField(this))
		text->append("value().");
	text->append(name_);
}

void BinaryIR::pregelPlus(Text *text, int op) const {
	lparen(text, op, op_->type);
	e1_->pregelPlus(text, op_->type);
	sprintf(buf, " %s ", getTokenStr(op_->type));
	text->append(string(buf));
	e2_->pregelPlus(text, op_->type);
	rparen(text, op, op_->type);
}

void FunctionIR::pregelPlus(Text *text, int op) const {
	text->append(name_);
	text->append("(");
	if (args_.size() > 0) {
		args_[0]->pregelPlus(text, tok_comma);
		for (int i = 1; i < args_.size(); i++) {
			text->append(", ");
			args_[i]->pregelPlus(text, tok_comma);
		}
	}
	text->append(")");
}

void NotIR::pregelPlus(Text *text, int op) const {
	lparen(text, op, tok_not);
	text->append("!");
	expr_->pregelPlus(text, tok_not);
	rparen(text, op, tok_not);
}

void NegativeIR::pregelPlus(Text *text, int op) const {
	lparen(text, op, tok_sub);
	text->append("-");
	expr_->pregelPlus(text, tok_sub);
	rparen(text, op, tok_sub);
}

void SelectIR::pregelPlus(Text *text, int op) const {
	expr_->pregelPlus(text, tok_dot);
	if (expr_->type()->type() == PalgolTypes::t_edge) {
		EdgeType *et = (EdgeType*) expr_->type();
		if (et->getSecond()->type() == PalgolTypes::t_unit)
			return; // single id field
	}
	if (op_->type == tok_fst)
		text->append(".first");
	if (op_->type == tok_snd)
		text->append(".second");
}

void SizeIR::pregelPlus(Text *text, int op) const {
	expr_->pregelPlus(text, tok_dot);
	text->append(".size()");
}

void ToiIR::pregelPlus(Text *text, int op) const {
	text->append("(int)");
	expr_->pregelPlus(text, tok_int);
}

void TofIR::pregelPlus(Text *text, int op) const {
	text->append("(double)");
	expr_->pregelPlus(text, tok_float);
}

void ReadMsgIR::pregelPlus(Text *text, int op) const {
	MessageManager *mm = getCodeGen()->getMM();
	ExprIR *data = mm->readMessage(ch_, index_, name_);
	data->pregelPlus(text, op);
}

void AggIR::pregelPlus(Text *text, int op) const {
	lparen(text, op, tok_dot);
	text->append("agg->" + name_);
	rparen(text, op, tok_dot);
}

void ExprStmtIR::pregelPlus(Text *text, int op) const {
	prtError("ExprStmtIR::pregelPlus(): should not reach");
}

void StmtListIR::pregelPlus(CodeGen *cg) const {
	for (int i = 0; i < list_.size(); i++)
		list_[i]->pregelPlus(cg);
}

void AssignIR::pregelPlus(CodeGen *cg) const {
	Text *text = new Text();
	bool hasDef = false;
	if (cg->needDef() && lval_->itype() == IRType::ir_var) {
		IREnv *env = getIREnv();
		VarIR *var = (VarIR*) lval_;
		if (!env->isField(var))
			if (!cg->hasDef(var)) {
				cg->defVar(var);
				if (var->type()->basic() || var->type()->type()
						== PalgolTypes::t_list)
					var->type()->toCPP(text);
				else
					text->append("auto");
				text->append(" ");
				hasDef = true;
			}
	}
	lval_->pregelPlus(text, 0);
	if (op_->type == tok_mmin || op_->type == tok_mmax ||
		op_->type == tok_mrand) {
		if (op_->type == tok_mmin)
			text->append(" = min(");
		if (op_->type == tok_mmax)
			text->append(" = max(");
		if (op_->type == tok_mrand)
			text->append(" = random(");
		lval_->pregelPlus(text, tok_comma);
		text->append(", ");
		expr_->pregelPlus(text, tok_comma);
		text->append(");");
	} else
	if (op_->type == tok_mapp || op_->type == tok_mrem) {
		if (op_->type == tok_mapp) {
			text->append(".push_back(");
			expr_->pregelPlus(text, tok_comma);
			text->append(");");
		} else {
			text->append(".erase(remove(");
			lval_->pregelPlus(text, 0);
			text->append(".begin(), ");
			lval_->pregelPlus(text, 0);
			text->append(".end(), ");
			expr_->pregelPlus(text, tok_comma);
			text->append("), ");
			lval_->pregelPlus(text, 0);
			text->append(".end());");
		}
	} else if (op_->type == tok_mass && expr_->itype() == IRType::ir_elist) {
		if (hasDef)
			text->append(";");
		else
			text->append(".clear();");
	} else {
		sprintf(buf, " %s ", getCppStr(op_->type));
		text->append(string(buf));
		expr_->pregelPlus(text, 0);
		text->append(";");
	}
	cg->add(text);
}

void RemoteIR::pregelPlus(CodeGen *cg) const {
	prtError("RemoteIR::pregelPlus(): should not reach");
}

void ForStmtIR::pregelPlus(CodeGen *cg) const {
	cg->incLoopLv();
	int lv = cg->loopLv();
	Type *ty = ((ListType*) expr_->type())->getContent();
	if (ty->basic()) {
		sprintf(buf, "for (auto %s : ", name_.c_str());
	} else {
		sprintf(buf, "for (const auto &%s : ", name_.c_str());
	}
	Text *text = new Text(string(buf));
	expr_->pregelPlus(text, tok_colon);
	text->append(string(")"));
	if (stmt_->single()) {
		cg->add(text);
		cg->add(new Increase());
		stmt_->pregelPlus(cg);
		cg->add(new Decrease());
	} else {
		text->append(" {");
		cg->add(text);
		cg->add(new Increase());
		stmt_->pregelPlus(cg);
		cg->add(new Decrease());
		cg->add(new Text("}"));
	}
	cg->decLoopLv();
}

void ProcedureIR::pregelPlus(CodeGen *cg) const {
	Text *text = new Text();
	func_->pregelPlus(text, 0);
	text->append(";");
	cg->add(text);
}

void SendMsgIR::pregelPlus(CodeGen *cg) const {
	Text *text = new Text();
	text->append("send_message(");
	dest_->pregelPlus(text, tok_comma);
	text->append(", ");
	ExprIR *expr = cg->getMM()->encMessage(this);
	expr->pregelPlus(text, tok_comma);
	text->append(");");
	cg->add(text);
}

void GetMsgIR::pregelPlus(CodeGen *cg) const {
	IREnv *env = getIREnv();
	if (!env->isField(var_)) {
		Text *def = new Text();
		if (var_->type() == nullptr)
			prtError("GetMsgIR::pregelPlus(): empty type");
		var_->type()->toCPP(def);
		sprintf(buf, " %s", var_->getName().c_str());
		def->append(string(buf));
		if (cg->getMM()->isUnique(ch_)) {
			// optimization, the message list contains a single element
			def->append(" = ");
			ExprIR *expr = cg->getMM()->readMessage(
					ch_, 0, string("msgs[0]"));
			expr->pregelPlus(def, 0);
			def->append(";");
			cg->add(def);
			return;
		}
		def->append(";");
		cg->add(def);
	}
	cg->add(new Text("for (const auto &msg : msgs)"));
	cg->add(new Increase());
	string m("msg");
	bool unq = cg->getMM()->isUnique(ch_);
	if (!unq) {
		Text *c = new Text(); // condition
		c->append("if (");
		ExprIR *ck = cg->getMM()->checkFlag(m, ch_);
		ck->pregelPlus(c, 0);
		c->append(")");
		cg->add(c);
		cg->add(new Increase());
	}
	Text *text = new Text();
	var_->pregelPlus(text, 0);
	text->append(" = ");
	ExprIR *expr = cg->getMM()->readMessage(ch_, 0, m);
	expr->pregelPlus(text, 0);
	text->append(";");
	cg->add(text);
	if (!unq)
		cg->add(new Decrease());
	cg->add(new Decrease());
}

void ScanStmtIR::pregelPlus(CodeGen *cg) const {
	cg->incLoopLv();
	int lv = cg->loopLv();
	if (cg->getMM()->isSimpleType())
		sprintf(buf, "for (auto %s : msgs)", name_.c_str());
	else
		sprintf(buf, "for (const auto &%s : msgs)", name_.c_str());
	Text *text = new Text(string(buf));
	bool unq = cg->getMM()->isUnique(ch_);
	if (!unq) {
		cg->add(text);
		cg->add(new Increase());
		text = new Text("if (");
		ExprIR *ck = cg->getMM()->checkFlag(name_.c_str(), ch_);
		ck->pregelPlus(text, 0);
		text->append(")");
	}
	if (stmt_->single()) {
		cg->add(text);
		cg->add(new Increase());
		stmt_->pregelPlus(cg);
		cg->add(new Decrease());
	} else {
		text->append(" {");
		cg->add(text);
		cg->add(new Increase());
		stmt_->pregelPlus(cg);
		cg->add(new Decrease());
		cg->add(new Text("}"));
	}
	if (!unq)
		cg->add(new Decrease());
	cg->decLoopLv();
}

void IfStmtIR::pregelPlus(CodeGen *cg) const {
	Text *text = new Text();
	text->append("if (");
	cond_->pregelPlus(text, 0);
	text->append(")");
	if (s2_ == nullptr && s1_->single()) {
		cg->add(text);
		cg->add(new Increase());
		s1_->pregelPlus(cg);
		cg->add(new Decrease());
		return;
	} else {
		text->append(" {");
		cg->add(text);
		cg->add(new Increase());
		s1_->pregelPlus(cg);
		cg->add(new Decrease());
	}
	if (s2_ != nullptr) {
		if (s2_->single()) {
			cg->add(new Text("} else"));
			cg->add(new Increase());
			s2_->pregelPlus(cg);
			cg->add(new Decrease());
			return;
		} else {
			cg->add(new Text("} else {"));
			cg->add(new Increase());
			s2_->pregelPlus(cg);
			cg->add(new Decrease());
		}
	}
	cg->add(new Text("}"));
}

void VoteToHalt::pregelPlus(CodeGen *cg) const {
	cg->add(new Text("vote_to_halt();"));
}

void ReturnIR::pregelPlus(CodeGen *cg) const {
	cg->add(new Text("return;"));
}

/* Implement the interface for class Conjunction
 *   void add(ExprIR *expr);
 *   StmtListIR *produce(StmtListIR *stmts);
 */
void Conjunction::add(ExprIR *expr) {
	preds_.push_back(expr);
}

StmtIR *Conjunction::produce(StmtIR *stmt) {
	if (preds_.size() == 0) return stmt;
	int cur = preds_.size() - 1;
	ExprIR *pred = preds_[cur--];
	for (; cur >= 0; cur--)
		pred = new BinaryIR(getBoolType(),
				getOper(tok_and), preds_[cur], pred);
	return new IfStmtIR(pred, new StmtListIR(stmt), nullptr);
}

/* Implement the interface for class ExprSum
 *   void add(ExprIR *expr);
 *   ExprIR *produce();
 */
void ExprSum::add(ExprIR *expr) {
	list_.push_back(expr);
}

ExprIR *ExprSum::produce() {
	if (list_.size() == 0)
		return new IntIR(0, 0);
	int cur = list_.size() - 1;
	ExprIR *sum = list_[cur--];
	for (; cur >= 0; cur--)
		sum = new BinaryIR(getIntType(),
				getOper(tok_add), list_[cur], sum);
	return sum;
}

/* implement the interface in class Superstep:
 *   void dump(FILE *f);
 */
void Superstep::dump(MessageManager *mm, FILE *f) {
	if (channels_.size() > 0) {
		fprintf(f, "| Channels:\n");
		for (auto ch : channels_) {
			fprintf(f, "|   C%d: ", ch);
			mm->getFstSM(ch)->dumpTypes(f);
		}
	}
	debug(stack_[0], f);
}

void Superstep::add(StmtIR *stmt) {
	stack_.back()->add(stmt);
}

void Superstep::push() {
	stack_.push_back(new StmtListIR());
}

StmtListIR *Superstep::get() const {
	return stack_.back();
}

StmtListIR *Superstep::pop() {
	auto ret = stack_.back();
	stack_.pop_back();
	return ret;
}

void Superstep::addChannel(int ch) {
	channels_.push_back(ch);
}

void Superstep::append(Superstep *s) {
	StmtListIR *stmts = s->get();
	for (int i = 0; i < stmts->size(); i++)
		stack_.back()->add(stmts->get(i));
	for (auto ch : s->channels_)
		channels_.push_back(ch);
}

/* Implement the interfaces in class STM */
void SeqSTM::dump(MessageManager *mm, FILE *f) {
	head_->dump(mm, f);
	if (next_ != nullptr)
		next_->dump(mm, f);
}

Superstep *SeqSTM::dupStep() {
    Superstep *ret = new Superstep();
    ret->append(head_);
    return ret;
}

void SeqSTM::append(SeqSTM *stm) {
	if (next_ == nullptr) {
		head_->append(stm->head_);
		next_ = stm->next_;
		jump_ = stm->jump_;
	} else
		next_->append(stm);
}

void SeqSTM::add(STM *stm) {
	if (next_ == nullptr) {
		next_ = stm;
	} else
		next_->add(stm);
}

void SeqSTM::setStep(CodeGen *cg, int step) {
	step_ = step;
	cg->setStepSTM(step, this);
	for (int ch : head_->getChannels())
		cg->getMM()->addChannelToStep(step, ch);
	if (next_ != nullptr)
		next_->setStep(cg, cg->nextStep());
}

void SeqSTM::pregelPlus(CodeGen *cg) const {
	cg->clearDefs();
	sprintf(buf, "if (phase == %d) {", step_);
	cg->add(new Text(buf));
	cg->add(new Increase());
	head_->get()->pregelPlus(cg);
	cg->addJump(jump(), step_);
    cg->add(new Text("return;"));
	cg->add(new Decrease());
	cg->add(new Text("}"));
	if (next_ != nullptr)
		next_->pregelPlus(cg);
}

void CondSTM::dump(MessageManager *mm, FILE *f) {
	debug(cond_, f);
	loop_->dump(mm, f);
	exit_->dump(mm, f);
}

void CondSTM::append(SeqSTM *stm) {
	exit_->append(stm);
}

void CondSTM::add(STM *stm) {
	exit_->add(stm);
}

void CondSTM::setStep(CodeGen *cg, int step) {
	step_ = step;
	cg->setStepSTM(step, this);
	loop_->setStep(cg, step);
	exit_->setStep(cg, step);
}

void CondSTM::pregelPlus(CodeGen *cg) const {
	sprintf(buf, "if (phase == %d) {", step_);
	cg->add(new Text(buf));
	cg->add(new Increase());
	Text *text = new Text("if (");
	cond_->pregelPlus(text, 0);
	text->append(") {");
	cg->add(text);
	cg->add(new Increase());
	cg->clearDefs();
	loop_->getStep()->get()->pregelPlus(cg);
	cg->addJump(loop_->jump(), step_);
	cg->add(new Decrease());
	cg->add(new Text("} else {"));
	cg->add(new Increase());
	cg->clearDefs();
	exit_->getStep()->get()->pregelPlus(cg);
	cg->addJump(exit_->jump(), step_);
	cg->add(new Decrease());
	cg->add(new Text("}"));
    cg->add(new Text("return;"));
	cg->add(new Decrease());
	cg->add(new Text("}"));
	if (loop_->getNext() != nullptr)
		loop_->getNext()->pregelPlus(cg);
	if (exit_->getNext() != nullptr)
		exit_->getNext()->pregelPlus(cg);
}

/* Implement the interfaces in class IREnv */
void IREnv::dump(FILE *f) {
	for (auto item : binds_) {
		fprintf(f, "----------\n");
		debug(item.second.first, f);
		debug(item.first, f);
		debug(item.second.second, f);
	}
	for (auto item : rbinds_) {
		fprintf(f, "----------\n");
		fprintf(f, "vector ");
		debug(item.second.first, f);
		debug(item.first, f);
		debug(item.second.second, f);
	}
}

void ExprSet::add(ExprAST *expr) {
	for (int i = 0; i < exprs_.size(); i++)
		if (expr->equals(exprs_[i]))
			return;
	exprs_.push_back(expr);
}

ExprAST *ExprSet::get(int index) {
	if (index < 0 || index >= exprs_.size())
		prtError("Exceeds the vector length");
	return exprs_[index];
}

int ExprSet::size() {
	return exprs_.size();
}

void IREnv::setCurVertex(Identifier *name) {
	curVertex_ = name;
}

Identifier *IREnv::getCurVertex() {
	return curVertex_;
}

void IREnv::addBinding(Identifier *name, ExprAST *expr) {
	stack_.back().insert(make_pair(name->getName(), expr));
}

ExprAST *IREnv::getExpr(Identifier *name) {
	for (auto it = stack_.rbegin(); it != stack_.rend(); it++) {
		auto fd = it->find(name->getName());
		if (fd != it->end()) return fd->second;
	}
	return nullptr;
}

int IREnv::isEdgeList(ExprAST *expr) {
	if (expr->etype() != EType::e_field) return 0;
	FieldExpr *fe = (FieldExpr *) expr;
	if (!fe->getExpr()->equals(curVertex_)) return 0;
	const char *name = fe->getName().c_str();
	if (!strcmp(name, "In" )) return 1;
	if (!strcmp(name, "Nbr")) return 2;
	if (!strcmp(name, "Out")) return 3;
	return 0;
}

void IREnv::getSubExprs(FieldExpr *fe, ExprSet *subs) {
	subs->add(fe->getExpr());
	if (fe->getExpr()->etype() == EType::e_field) {
		FieldExpr *subfe = (FieldExpr*) fe->getExpr();
		getSubExprs(subfe, subs);
	}
}

bool IREnv::isReqResp(FieldExpr *fe) {
	if (fe->getExpr()->etype() == EType::e_field) {
		FieldExpr *subfe = (FieldExpr*) fe->getExpr();
		return (subfe->getExpr()->equals(curVertex_));
	}
	return false;
}

ExprIR *IREnv::transformRemote(ExprAST *vertex) {
	if (vertex->equals(curVertex_))
		return new IDIR();
	// read cache
	VarIR *cache = rfind(vertex);
	if (cache != nullptr) return cache;
	// calculate
	if (vertex->etype() == EType::e_field) {
		FieldExpr *fe = (FieldExpr*) vertex;
		ExprSet *subs = new ExprSet();
		getSubExprs(fe, subs);
		int minDepth = 1000;
		VarIR *result = nullptr;
		for (int i = 0; i < subs->size(); i++) {
			FieldExpr *dest = fe->replace(subs->get(i), curVertex_);
			ExprIR *c = transformRemote(subs->get(i));
			ExprIR *d = dest->tr_loc(this);
			int curDepth = max(c->depth(), d->depth()) + 1;
			if (curDepth < minDepth) {
				minDepth = curDepth;
				result = rbind(vertex, new SendPatt(c, d));
			}
		}
		return result;
	} else {
		ExprIR *dest = vertex->tr_loc(this);
		dest->calcDepth(this);
		VarIR *var = new VarIR(getVidType(), createVar(), dest->depth());
		vbind(var, dest);
		return rbind(vertex, new SendPatt(new IDIR(), var));
	}
}

VarIR *IREnv::bind(FieldExpr *fe, SendPatt *expr) {
	VarIR *var = new VarIR(expr->type(), createVar(), expr->depth());
	binds_.push_back(make_pair(fe, make_pair(var, expr)));
	return var;
}

VarIR *IREnv::find(FieldExpr *fe) {
	for (auto item : binds_)
		if (fe->equals(item.first))
			return item.second.first;
	return nullptr;
}

VarIR *IREnv::rbind(ExprAST *vertex, SendPatt *expr) {
	VarIR *var = new VarIR(expr->type(), createVar(), expr->depth());
	rbinds_.push_back(make_pair(vertex, make_pair(var, expr)));
	return var;
}

void IREnv::vbind(VarIR *var, ExprIR *expr) {
	vbinds_.insert(make_pair(var->getName(), expr));
}

VarIR *IREnv::rfind(ExprAST *vertex) {
	for (auto item : rbinds_)
		if (vertex->equals(item.first))
			return item.second.first;
	return nullptr;
}

string IREnv::createVar() {
	static char buf[20];
	sprintf(buf, "t%d", ++varid_);
	return string(buf, buf + strlen(buf));
};

void IREnv::pushStmts() {
	stmts_.push_back(new StmtListIR());
}

StmtListIR *IREnv::popStmts() {
	StmtListIR *ret = stmts_.back();
	stmts_.pop_back();
	return ret;
}

void IREnv::emit(StmtIR *stmt) {
	stmts_.back()->add(stmt);
}

LValue *IREnv::getLValue(Type *type) {
	if (lval_ == nullptr)
		return new VarIR(type, createVar());
	else {
		LValue *ret = lval_;
		lval_ = nullptr;
		return ret;
	}
}

Operator *IREnv::getOperator() {
	if (op_ == nullptr)
		return getOper(tok_mass);
	else {
		Operator *ret = op_;
		op_ = nullptr;
		return ret;
	}
}

void IREnv::useVarIR(VarIR *var) {
	used_.insert(var->getName());
}

SendPatt *IREnv::getSendPatt(VarIR *var) {
	for (auto item : binds_)
		if (item.second.first->getName() == var->getName())
			return item.second.second;
	for (auto item : rbinds_)
		if (item.second.first->getName() == var->getName())
			return item.second.second;
	prtError("IREnv::getSendPatt(): variable not found");
	return nullptr;
}

void IREnv::produceVariable(VarIR *var) {
	if (calc_.find(var->getName()) != calc_.end()) return;
	ExprIR *expr = vbinds_[var->getName()];
	int step = expr->depth();
	if (expr->itype() == IRType::ir_exst)
		((ExprStmtIR*)expr)->getStmts()->genSteps(this, step);
	else
		addToStep(step, new AssignIR(var, getOper(tok_mass), expr));
	calc_.insert(make_pair(var->getName(), -1));	
	useVarInStep(var, step);
}

void IREnv::producePattOne(VarIR *var) {
	if (calc_.find(var->getName()) != calc_.end()) return;
	SendPatt *send = getSendPatt(var);
	int step = var->depth(); // get VarIR at this step
	ExprIR *cont = send->cont();
	ExprIR *dest = send->dest();
	IRType ct = cont->itype();
	IRType dt = dest->itype();
	if (ct == IRType::ir_var)
		producePattOne((VarIR*) cont);
	int ch = producePattTwo((VarIR*) dest);
	int newCh = newChannel();
	SendMsgIR *sm = new SendMsgIR(newCh, vector<ExprIR*>(1, cont),
			new ReadMsgIR(getVidType(), "msg", ch, 0));
	getMM()->addSM(sm);
	addToStep(step - 1, new ScanStmtIR(ch, "msg", nullptr, sm));
	addToStep(step, new GetMsgIR(newCh, var));
	addChannel(step, newCh);
	calc_.insert(make_pair(var->getName(), -1));
	useVarInStep(var, step);
}

int IREnv::producePattTwo(VarIR *var) {
	if (calc_.find(var->getName()) != calc_.end())
		return calc_[var->getName()]; // channel number
	SendPatt *send = getSendPatt(var);
	int step = var->depth();
	ExprIR *cont = send->cont();
	ExprIR *dest = send->dest();
	IRType ct = cont->itype();
	IRType dt = dest->itype();
	if (dt != IRType::ir_var && dt != IRType::ir_field)
		prtError("IREnv::producePattTwo(): something wrong with destination");
	if (dt == IRType::ir_var) {
		// two cases, either pattern one or u's local expression
		VarIR *d = (VarIR*) dest;
		if (vbinds_.find(d->getName()) != vbinds_.end())
			produceVariable(d);
		else
			producePattOne(d);
	}
	int newCh = newChannel();
	if (ct == IRType::ir_var) {
		int ch = producePattTwo((VarIR*) cont);
		ReadMsgIR *rm = new ReadMsgIR(getVidType(), "msg", ch, 0);
		SendMsgIR *sm = new SendMsgIR(newCh, vector<ExprIR*>(1, rm) , dest);
		getMM()->addSM(sm);
		addToStep(step - 1, new ScanStmtIR(ch, var->getName(), nullptr, sm));
	} else {
		SendMsgIR *sm = new SendMsgIR(newCh,
				vector<ExprIR*>(1, cont), dest, CombinerT::c_null);
		getMM()->addSM(sm);
		addToStep(step - 1, sm);
	}
	addChannel(step, newCh);
	calc_.insert(make_pair(var->getName(), newCh));
	useVarInStep(var, step);
	return newCh;
}

void IREnv::initSteps(int steps) {
	hasRU_ = false;
	for (int i = 0; i <= steps; i++)
		steps_.push_back(new Superstep());
	for (auto item : binds_) {
		VarIR *var = item.second.first;
		if (used_.find(var->getName()) == used_.end())
			continue;
		producePattOne(var);
	}
	for (auto item : binds_) {
		VarIR *var = item.second.first;
		if (used_.find(var->getName()) == used_.end())
			continue;
		producePattTwo(var);
	}
	used_.clear();
	calc_.clear();
}

void IREnv::addToStep(int step, StmtIR *stmt) {
	steps_[step]->add(stmt);
}

void IREnv::addChannel(int step, int ch) {
	steps_[step]->addChannel(ch);
}

int IREnv::remoteUpdate(FieldName *fn, Type *type, Operator *op) {
	hasRU_ = true;
	auto v = rus_.find(fn->getName());
	if (v == rus_.end()) {
		int ch = newChannel();
		rus_.insert(make_pair(fn->getName(),
				new RUStruct(ch, op->type, fn, type)));
		return ch;
	}
	if (op->type != v->second->op)
		prtError(*op, "update remote field with different operators");
	return v->second->ch;
}

SeqSTM *IREnv::produceSteps(bool stop) {
	SeqSTM *prog = nullptr;
	if (hasRU_) {
		Superstep *step = new Superstep();
		for (auto it : rus_) {
			RUStruct *rus = it.second;
			FieldIR *fn = new FieldIR(rus->type, rus->fn);
			AssignIR *ass = new AssignIR(fn, getOper(rus->op),
					new ReadMsgIR(rus->type, "msg", rus->ch, 0));
			step->add(new ScanStmtIR(rus->ch, "msg", nullptr, ass));
			step->addChannel(rus->ch);
		}
		prog = new SeqSTM(false, step, prog);
	}
	prog = new SeqSTM(false, steps_.back(), prog);
	int size = steps_.size();
	for (int i = size - 2; i >= 0; i--)
		prog = new SeqSTM(true, steps_[i], prog);
	return prog;
}

void IREnv::useVarInStep(const VarIR *var, int step) {
	if (strncmp(var->getName().c_str(), "t", 1) != 0) return;
	auto v = varStep_.find(var->getName());
	if (v == varStep_.end()) {
		varStep_.insert(make_pair(var->getName(),
				make_pair(var->type(), make_pair(step, step))));
	} else {
		Type *t = v->second.first;
		int l = min(v->second.second.first,  step);
		int h = max(v->second.second.second, step);
		varStep_[var->getName()] = make_pair(t, make_pair(l, h));
	}
}

bool IREnv::isField(const VarIR *var) {
	if (isf_.find(var->getName()) != isf_.end())
		return true;
	auto v = varStep_.find(var->getName());
	if (v == varStep_.end())
		return false;
	pair<int, int> p = v->second.second;
	return p.first != p.second;
}

void IREnv::turnToField(const string &name) {
	isf_.insert(name);
}

void IREnv::checkVarsSpans() {
	for (auto v : varStep_) {
		pair<int, int> p = v.second.second;
		if (p.first != p.second)
			useField(new FieldIR(v.second.first,
				new FieldName(v.first.c_str())));
	}
}

void IREnv::useField(FieldIR *fn) {
	getVM()->add(fn);
}

void IREnv::pushIterField(FieldName *fn) {
	itcnt_[fn->getName()] += 1;
}

void IREnv::popIterField(FieldName *fn) {
	itcnt_[fn->getName()] -= 1;
}

string IREnv::genIterField(FieldName *fn) {
	string ret = "o";
	ret.push_back(itcnt_[fn->getName()] + '0');
	return ret + fn->getName();
}

string IREnv::genCountField() {
	sprintf(buf, "cnt%d", repeat_);
	return string(buf);
}

ExprIR *IREnv::registerAgg(Type *type, Operator *op,
		Conjunction *conj, ExprIR *expr) {
	string ret = getAM()->add(new AggInfo(type, op, conj, expr));
	return new AggIR(type, ret);
}

FieldIR *IREnv::getAggFieldIR() {
	FieldIR *ret = getAM()->getAggFieldIR();
	useField(ret);
	return ret;
}

void IREnv::clear() {
	binds_.clear();
	rbinds_.clear();
	vbinds_.clear();
	steps_.clear();
	used_.clear();
	calc_.clear();
	rus_.clear();
	isf_.clear();
}

AggregatorManager *IREnv::getAM() {
	if (am_ == nullptr)
		am_ = new AggregatorManager();
	return am_;
}

MessageManager *IREnv::getMM() {
	if (mm_ == nullptr)
		mm_ = new MessageManager();
	return mm_;
}

VertexManager *IREnv::getVM() {
	if (vm_ == nullptr)
		vm_ = new VertexManager();
	return vm_;
}

/* Implement other functions
 */
static IREnv *env = new IREnv();
SeqSTM *transform(VCProg *prog, bool reqresp, bool dump) {
	Config *conf = getConfig();
	prog->confCheck();
	if (!conf->noInitCheck) {
		set<string> inps = getCodeGen()->getInputFields();
		vector<FieldName*> oups = getCodeGen()->getOutputFNs();
		prog->initCheck(inps);
		for (auto fn : oups) {
			if (inps.find(fn->getName()) == inps.end()) {
				if (fn->getName() == "In" || fn->getName() == "Out") {
					getConfig()->genSymEdge = true;
				} else {
					// other list do not need to initialize
					if (getTypeEnv()->getType(fn)->type() !=
							PalgolTypes::t_list)
						prtInitError(*fn, fn->getName().c_str());
				}
			}
		}
	}
	SeqSTM *ir = prog->transform(env);
	// append voteToHalt()
	Superstep *last = new Superstep();
	last->add(new VoteToHalt());
	SeqSTM *e = new SeqSTM(false, last, nullptr);
	ir->append(e);
	// calclate used field
	env->checkVarsSpans();
	env->useField(new FieldIR(getIntType(),
			new FieldName("phase")));
	if (dump)
		ir->dump(env->getMM());
	return ir;
}

IREnv *getIREnv() {
	return env;
}

void debug(const IR *ir, FILE *f) {
	ir->print(f);
	fprintf(f, "\n");
}
