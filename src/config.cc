#include <cstdlib>
#include <cstring>

#include "config.h"

static Config *conf = new Config();
Config *getConfig() {
	return conf;
}

bool Config::hasReqResp() const {
	return (!strcmp(target, "pregel+") && !strcmp(mode, "reqresp"))
		|| (!strcmp(target, "custom"));
}
