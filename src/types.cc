#include <cstdio>

#include "ast.h"
#include "codegen.h"
#include "errorinfo.h"
#include "intermediate.h"
#include "types.h"

using namespace std;

static char buf[128];

void TypeEnv::Scope::add(Identifier *name, Type *t, bool mut) {
	types_.insert(make_pair(name->getName(), t));
	if (mut) mut_.insert(name->getName());
}

Type *TypeEnv::Scope::get(Identifier *name) {
	auto v = types_.find(name->getName());
	return (v == types_.end() ? nullptr : v->second);
}

bool TypeEnv::Scope::exists(Identifier *name) {
	return mut_.find(name->getName()) != mut_.end();
}

TypeEnv::TypeEnv() {
	inc_ = 0;
	ver_ = 0;
	w_ = createTypeVar();
	alias_.insert(make_pair(string("int"), getIntType()));
	alias_.insert(make_pair(string("vid"), getVidType()));
	alias_.insert(make_pair(string("bool"), getBoolType()));
	alias_.insert(make_pair(string("unit"), getUnitType()));
	alias_.insert(make_pair(string("float"), getFloatType()));
}

Type *TypeEnv::lookup(TypeVar *vt) {
	auto v = table_.find(vt->getId());
	if (v == table_.end())
		return vt;
	else
		return v->second;
}

void TypeEnv::extend(TypeVar *vt, Type *t, Pos pos) {
	if (!vt->equals(t) && t->contains(vt))
		prtError("infinite type");
	for (auto rec = table_.begin(); rec != table_.end(); rec++)
		if (rec->second->equals(vt))
			rec->second = t;
		else
			rec->second->replace(vt, t);
	table_.insert(make_pair(vt->getId(), t));
}

TypeVar *TypeEnv::createTypeVar() {
	return new TypeVar(++inc_);
}

void TypeEnv::addTypeAlias(Identifier *str, Type *t) {
	if (alias_.find(str->getName()) != alias_.end())
		prtError(*str, "redefinition of type alias");
	alias_.insert(make_pair(str->getName(), t));
}

Type *TypeEnv::getTypeAlias(Identifier *str) {
	auto v = alias_.find(str->getName());
	if (v == alias_.end())
		prtError(*str, "undefined type alias");
	return v->second;
}

void TypeEnv::addExternal(Identifier *name, vector<Type*> tl) {
	exts_.insert(make_pair(name->getName(), tl));
}

bool TypeEnv::existsFunc(Identifier *name) {
	return exts_.find(name->getName()) != exts_.end();
}

void TypeEnv::suggestCPP(FILE *f) {
	if (exts_.empty()) return;
	for (const auto &func : exts_) {
		fprintf(f, "* function \"%s\"\n", func.first.c_str());
		const vector<Type*> &vec = func.second;
		for (int i = 0; i + 1 < vec.size(); i++) {
			sprintf(buf, " - arg%d: ", i);
			Text *text = new Text(buf);
			vec[i]->toCPP(text);
			text->print(0, f);
			delete text;
		}
		sprintf(buf, " return: ");
		Text *text = new Text(buf);
		vec.back()->toCPP(text);
		text->print(0, f);
		fprintf(f, "\n");
		delete text;
	}
}
 
vector<Type*> TypeEnv::getFuncType(Identifier *name) {
	return exts_[name->getName()];
}

void TypeEnv::addBinding(Identifier *name, Type *t, bool mut) {
	stack_.back()->add(name, t, mut);
	name->produceType(this);
}

bool TypeEnv::exists(Identifier *name) {
	for (auto it = stack_.rbegin(); it != stack_.rend(); it++) {
		if ((*it)->get(name) != nullptr)
			return true;
		if (!(*it)->recur_) break;
	}
	return false;
}

Type *TypeEnv::getType(Identifier *name) {
	for (auto it = stack_.rbegin(); it != stack_.rend(); it++) {
		Type *t = (*it)->get(name);
		if (t != nullptr) return t;
		if (!(*it)->recur_) break;
	}
	return nullptr;
}

bool TypeEnv::isMutable(Identifier *name) {
	for (auto it = stack_.rbegin(); it != stack_.rend(); it++) {
		if ((*it)->exists(name))
			return true;
		if (!(*it)->recur_) break;
	}
	return false;
}

void TypeEnv::setType(FieldName *field, Type *t) {
	auto v = globals_.find(field->getName());
	const char *s = field->getName().c_str();
	bool isNbr = !strcmp(s, "Nbr");
	bool isInOut = !strcmp(s, "In") || !strcmp(s, "Out");
	if (v != globals_.end() && !isInOut &&
			!field->equals(getIREnv()->getAM()->getAggFieldName())) {
		sprintf(buf, "redefinition of the field %s", 
				field->getName().c_str());
		prtError(buf);
	}
	if (isInOut || isNbr) {
		addConstraint(getType(field), t, *field);
	} else
		globals_.insert(make_pair(field->getName(), t));
}

Type *TypeEnv::getType(FieldName *field) {
	auto v = globals_.find(field->getName());
	if (v == globals_.end()) {
		const char *s = field->getName().c_str();
		// Currently, 'Id' is supposed to have type vid
		if (!strcmp(s, "Id"))
			return getVidType();
		bool isNbr = !strcmp(s, "Nbr");
		bool isInOut = !strcmp(s, "In") || !strcmp(s, "Out");
		Type *t = nullptr;
		if (isNbr || isInOut)
			t = new ListType(new EdgeType(getVidType(), w_));
		else
			t = createTypeVar();
		if (isInOut) {
			globals_.insert(make_pair(string("In"), t));
			globals_.insert(make_pair(string("Out"), t));
		} else
			globals_.insert(make_pair(field->getName(), t));
		return t;
	}
	return v->second->subst(this);
}

void TypeEnv::addField(FieldName *field) {
	if (fpos_.find(field->getName()) == fpos_.end())
		fpos_.insert(make_pair(field->getName(), *field));
}

void TypeEnv::addNumeric(Type *t, Pos pos) {
	numeric_.push_back(make_pair(pos, t));
}

void TypeEnv::addComparable(Type *t, Pos pos) {
	compa_.push_back(make_pair(pos, t));
}

void TypeEnv::addEdgeWeight(Type *t, Pos pos) {
	if (t->type() == PalgolTypes::t_var) {
		TypeVar *tv = (TypeVar *) t;
		weight_.insert(make_pair(tv->getId(), tv));
	}
}

void TypeEnv::addConstraint(Type *t1, Type *t2, Pos pos) {
	if (t1->equals(t2)) return;
	if (t1->type() == PalgolTypes::t_var) {
		Type *v1 = t1->subst(this);
		Type *v2 = t2->subst(this);
		if (t1->equals(v1))
			extend((TypeVar*) v1, v2, pos);
		else
			addConstraint(v1, v2, pos);
	} else
	if (t2->type() == PalgolTypes::t_var) {
		addConstraint(t2, t1, pos);
	} else {
		if (t1->type() != t2->type()) {
			fprintf(stderr, "expected type : ");
			debug(t1);
			fprintf(stderr, "  actual type : ");
			debug(t2);
			prtTypeError(pos, "unmatched types");
		}
		if (t1->type() == PalgolTypes::t_pair) {
			PairType *p1 = (PairType*) t1;
			PairType *p2 = (PairType*) t2;
			addConstraint(p1->getFirst(),  p2->getFirst(),  pos);
			addConstraint(p1->getSecond(), p2->getSecond(), pos);
		}
		if (t1->type() == PalgolTypes::t_edge) {
			EdgeType *p1 = (EdgeType*) t1;
			EdgeType *p2 = (EdgeType*) t2;
			addConstraint(p1->getFirst(),  p2->getFirst(),  pos);
			addConstraint(p1->getSecond(), p2->getSecond(), pos);
		}
		if (t1->type() == PalgolTypes::t_list) {
			ListType *l1 = (ListType*) t1;
			ListType *l2 = (ListType*) t2;
			addConstraint(l1->getContent(), l2->getContent(), pos);
		}
	}
}

void TypeEnv::trace(ExprAST *expr) {
	trace_.push_back(expr);
}

void TypeEnv::check() {
	// infer those types with numeric constraint
	for (int i = 0; i < numeric_.size(); i++) {
		Type *t = numeric_[i].second->subst(this);
		if (t->type() == PalgolTypes::t_var)
			addConstraint(t, getIntType(), Pos());
		else if (!t->numeric())
			prtTypeError(numeric_[i].first, "violate numeric constraint");
	}
	// befor inferring the types of edge weights
	// remove those that appeared in the program
	for (int i = 0; i < trace_.size(); i++) {
		trace_[i]->subst(this);
		Type *t = trace_[i]->type();
		if (t->type() == PalgolTypes::t_var)
			weight_.erase(((TypeVar*) t)->getId());
	}
	// infer edge weights (not decided -> unit)
	for (const auto &w : weight_) {
		Type *t = w.second->subst(this);
		if (t->type() == PalgolTypes::t_var)
			addConstraint(t, getUnitType(), Pos());
	}
	// check whether all fields are well-typed
	for (auto fn : fpos_) {
		if (fn.first == "Id") continue;
		auto ft = globals_.find(fn.first);
		bool cannot = (ft == globals_.end());
		if (!cannot) {
			Type *t = ft->second->subst(this);
			cannot = t->contains(PalgolTypes::t_var);
			if (cannot) {
				fprintf(stderr, "interred type: ");
				debug(t);
			}
		}
		if (cannot) {
			sprintf(buf, "cannot infer the type of field '%s'",
					fn.first.c_str());
			prtError(fn.second, buf);
		}
	}
	// check whether those comparable types are well-types
	for (int i = 0; i < compa_.size(); i++) {
		Type *t = compa_[i].second->subst(this);
		if (!t->comparable()) {
			fprintf(stderr, "inferred type: ");
			debug(t);
			prtTypeError(compa_[i].first, "cannot compare instances in this type");
		}
	}
	// check whether there are ill-typed expressions
	for (int i = 0; i < trace_.size(); i++) {
		trace_[i]->subst(this);
		Type *t = trace_[i]->type();
		if (t->type() == PalgolTypes::t_unit)
			prtTypeError(*trace_[i], "cannot access element with unit type");
		if (t->contains(PalgolTypes::t_var))
			prtTypeError(*trace_[i], "cannot infer the type");
	}
}

void TypeEnv::dump(FILE *f) {
	fprintf(f, "Fields and their types:\n");
	for (auto const& name: globals_) {
		fprintf(f, " %s: ", name.first.c_str());
		debug(name.second->subst(this), f);
	}
}

int TypeVar::getId() const {
	return id_;
}

Type *PairType::getFirst() {
	return fst_;
}

Type *PairType::getSecond() {
	return snd_;
}

Type *ListType::getContent() {
	return cont_;
}

Type *SetType::getContent() {
	return cont_;
}

/* implement the interface in class Type:
 *   Type *subst(TypeEnv *env);
 */
Type *TypeVar::subst(TypeEnv *env) {
	return env->lookup(this);
}

Type *PairType::subst(TypeEnv *env) {
	return new PairType(fst_->subst(env), snd_->subst(env));
}

Type *EdgeType::subst(TypeEnv *env) {
	return new EdgeType(fst_->subst(env), snd_->subst(env));
}

Type *ListType::subst(TypeEnv *env) {
	return new ListType(cont_->subst(env));
}

Type *SetType::subst(TypeEnv *env) {
	return new SetType(cont_->subst(env));
}

/**
 * initialize static in-class members;
 * access to the unique class instance
 */
IntType *IntType::itype = new IntType();
VidType *VidType::vtype = new VidType();
BoolType *BoolType::btype = new BoolType();
UnitType *UnitType::utype = new UnitType();
FloatType *FloatType::ftype = new FloatType();

IntType *getIntType() {
	return IntType::itype;
}

VidType *getVidType() {
	return VidType::vtype;
}

BoolType *getBoolType() {
	return BoolType::btype;
}

UnitType *getUnitType() {
	return UnitType::utype;
}

FloatType *getFloatType() {
	return FloatType::ftype;
}

/* implement the interface in class Type:
 *   bool equals(Type *t);
 */
bool TypeVar::equals(Type *t) const {
	return (type_ == t->type() && id_ == ((TypeVar*) t)->getId());
}

bool PairType::equals(Type *t) const {
	if (type_ != t->type())
		return false;
	PairType *pt = (PairType*) t;
	return fst_->equals(pt->fst_) && snd_->equals(pt->snd_);
}

bool ListType::equals(Type *t) const {
	return (type_ == t->type() &&
			cont_->equals(((ListType*) t)->cont_));
}

bool SetType::equals(Type *t) const {
	return (type_ == t->type() &&
			cont_->equals(((SetType*) t)->cont_));
}

/* implement the interface in class Type:
 *   void print(FILE *f);
 */
void UnitType::print(FILE *f) const {
	fprintf(f, "()");
}

void TypeVar::print(FILE *f) const {
	fprintf(f, "t%d", id_);
}

void PairType::print(FILE *f) const {
	fprintf(f, "(");
	fst_->print(f);
	fprintf(f, ", ");
	snd_->print(f);
	fprintf(f, ")");
}

void EdgeType::print(FILE *f) const {
	fprintf(f, "{");
	fst_->print(f);
	fprintf(f, ", ");
	snd_->print(f);
	fprintf(f, "}");
}

void ListType::print(FILE *f) const {
	fprintf(f, "[");
	cont_->print(f);
	fprintf(f, "]");
}

void SetType::print(FILE *f) const {
	fprintf(f, "set(");
	cont_->print(f);
	fprintf(f, ")");
}

void VidType::print(FILE *f) const {
	fprintf(f, "vid");
}

void IntType::print(FILE *f) const {
	fprintf(f, "int");
}

void FloatType::print(FILE *f) const {
	fprintf(f, "float");
}

void BoolType::print(FILE *f) const {
	fprintf(f, "bool");
}

/* implement the interface in class Type:
 *   void toCPP(Text *text) const;
 */
void TypeVar::toCPP(Text *text) const {
	fail("TypeVar::toCPP(): should not reach");
}

void PairType::toCPP(Text *text) const {
	text->append("pair<");
	fst_->toCPP(text);
	text->append(", ");
	snd_->toCPP(text);
	if (text->back() == '>')
		text->append(" ");
	text->append(">");
}

void EdgeType::toCPP(Text *text) const {
	if (snd_->type() != PalgolTypes::t_unit)
		PairType::toCPP(text);
	else
		text->append("int");
}

void ListType::toCPP(Text *text) const {
	text->append("vector<");
	cont_->toCPP(text);
	if (text->back() == '>')
		text->append(" ");
	text->append(">");
}

void SetType::toCPP(Text *text) const {
	text->append("set<");
	cont_->toCPP(text);
	if (text->back() == '>')
		text->append(" ");
	text->append(">");
}

void UnitType::toCPP(Text *text) const {
	text->append("void");
}

void VidType::toCPP(Text *text) const {
	text->append("int");
}

void IntType::toCPP(Text *text) const {
	text->append("int");
}

void FloatType::toCPP(Text *text) const {
	text->append("double");
}

void BoolType::toCPP(Text *text) const {
	text->append("bool");
}

/* implement the interface in class Type:
 *   bool contains(TypeVar *vt) const;
 */
bool TypeVar::contains(TypeVar *vt) const {
	return id_ == vt->id_;
}

bool PairType::contains(TypeVar *vt) const {
	return fst_->contains(vt) || snd_->contains(vt);
}

bool ListType::contains(TypeVar *vt) const {
	return cont_->contains(vt);
}

bool SetType::contains(TypeVar *vt) const {
	return cont_->contains(vt);
}

/* implement the interface in class Type:
 *   bool contains(int type);
 */
bool PairType::contains(PalgolTypes type) const {
	return (type_ == type || fst_->contains(type) || snd_->contains(type));
}

bool ListType::contains(PalgolTypes type) const {
	return (type_ == type || cont_->contains(type));
}

bool SetType::contains(PalgolTypes type) const {
	return (type_ == type || cont_->contains(type));
}

/* implement the interface in class Type:
 *   void replace(TypeVar *vt, Type *t);
 */
void PairType::replace(TypeVar *vt, Type *t) {
	if (fst_->equals(vt))
		fst_ = t;
	else
		fst_->replace(vt, t);
	if (snd_->equals(vt))
		snd_ = t;
	else
		snd_->replace(vt, t);
}

void ListType::replace(TypeVar *vt, Type *t) {
	if (cont_->equals(vt))
		cont_ = t;
	else
		cont_->replace(vt, t);
}

void SetType::replace(TypeVar *vt, Type *t) {
	if (cont_->equals(vt))
		cont_ = t;
	else
		cont_->replace(vt, t);
}

/* implement the interface in class Type:
 *   bool numeric() const;
 */
bool IntType::numeric() const {
	return true;
}

bool FloatType::numeric() const {
	return true;
}

bool VidType::numeric() const {
	return false;
}

/* implement the interface in class Type:
 *   bool comparable() const;
 */
bool UnitType::comparable() const {
	return true;
}

bool TypeVar::comparable() const {
	return false;
}

bool ListType::comparable() const {
	return false;
}

bool SetType::comparable() const {
	return false;
}

bool PairType::comparable() const {
	return fst_->comparable() && snd_->comparable();
}

/* implement the interface in class Type:
 *   void flatten(vector<BasicType*> &v);
 */
Type *TypeVar::flatten(vector<BasicType*> &v) {
	fail("Typevar::flatten(): should not reach");
	return nullptr;
}

Type *PairType::flatten(vector<BasicType*> &v) {
	return new PairType(fst_->flatten(v), snd_->flatten(v));
}

Type *EdgeType::flatten(vector<BasicType*> &v) {
	if (snd_->type() != PalgolTypes::t_unit)
		return new PairType(fst_->flatten(v), snd_->flatten(v));
	else
		return fst_->flatten(v);
}

Type *ListType::flatten(vector<BasicType*> &v) {
	fail("list cannot be sent via the messages");
	return nullptr;
}

Type *SetType::flatten(vector<BasicType*> &v) {
	fail("set cannot be sent via the messages");
	return nullptr;
}

Type *VidType::flatten(vector<BasicType*> &v) {
	BasicType *t = new VidType(); // create new instance
	v.push_back(t);
	return t;
}

Type *IntType::flatten(vector<BasicType*> &v) {
	BasicType *t = new IntType(); // create new instance
	v.push_back(t);
	return t;
}

Type *FloatType::flatten(vector<BasicType*> &v) {
	BasicType *t = new FloatType(); // create new instance
	v.push_back(t);
	return t;
}

Type *BoolType::flatten(vector<BasicType*> &v) {
	BasicType *t = new BoolType(); // create new instance
	v.push_back(t);
	return t;
}

/* implement the interface in class Type:
 *   ExprIR *produceRead(const char *name);
 */
ExprIR *PairType::produceRead(const char *name, bool sp) {
	return new PairIR(this, fst_->produceRead(name, sp),
			snd_->produceRead(name, sp));
}

ExprIR *VidType::produceRead(const char *name, bool sp) {
	IntValue *enc = (IntValue*) enc_;
	if (sp) sprintf(buf, "%s", name);
	else sprintf(buf, "%s.i%d", name, enc->index);
	VarIR *var = new VarIR(this, string(buf));
	if (enc->start != 0)
		return new BinaryIR(this, getOper(tok_shr), 
				var, new IntIR(enc->start, 0));
	else return var;
}

ExprIR *IntType::produceRead(const char *name, bool sp) {
	IntValue *enc = (IntValue*) enc_;
	if (sp) sprintf(buf, "%s", name);
	else sprintf(buf, "%s.i%d", name, enc->index);
	VarIR *var = new VarIR(this, string(buf));
	if (enc->start != 0)
		return new BinaryIR(this, getOper(tok_shr), 
				var, new IntIR(enc->start, 0));
	else return var;
}

ExprIR *FloatType::produceRead(const char *name, bool sp) {
	FloatValue *enc = (FloatValue*) enc_;
	if (sp) sprintf(buf, "%s", name);
	else sprintf(buf, "%s.d%d", name, enc->index);
	return new VarIR(this, string(buf));
}

ExprIR *BoolType::produceRead(const char *name, bool sp) {
	IntValue *enc = (IntValue*) enc_;
    if (sp) sprintf(buf, "%s", name);
    else sprintf(buf, "%s.i%d", name, enc->index);
	VarIR *var = new VarIR(this, string(buf));
	if (enc->start != 0)
		// 1 & (var >> start)
		return new BinaryIR(this, getOper(tok_bita),
			new BinaryIR(getIntType(), getOper(tok_shr), 
				var, new IntIR(enc->start, 0)),
			new IntIR(1, 0));
	else // 1 & var
		return new BinaryIR(this, getOper(tok_bita),
			var, new IntIR(1, 0));
}

/* implement the interface in class Type:
 *   void produce(ExprSum *sums[], ExprIR *floats[], ExprIR *e);
 */
void PairType::produceParam(ExprSum *sums[], ExprIR *floats[], ExprIR *e) {
	ExprIR *e1 = e->tr_readmsg();
	if (e1->itype() == IRType::ir_pair) {
		PairIR *p = (PairIR*) e1;
		fst_->produceParam(sums, floats, p->getFirst());
		snd_->produceParam(sums, floats, p->getSecond());
	} else {
		fst_->produceParam(sums, floats, new SelectIR(fst_, getOper(tok_fst), e1));
		snd_->produceParam(sums, floats, new SelectIR(snd_, getOper(tok_snd), e1));
	}
}

void VidType::produceParam(ExprSum *sums[], ExprIR *floats[], ExprIR *e) {
	IntValue *enc = (IntValue*) enc_;
	if (enc->start != 0)
		sums[enc->index]->add(new BinaryIR(getIntType(), 
			getOper(tok_shl), e, new IntIR(enc->start, 0)));
	else
		sums[enc->index]->add(e);
}

void IntType::produceParam(ExprSum *sums[], ExprIR *floats[], ExprIR *e) {
	IntValue *enc = (IntValue*) enc_;
	if (enc->start != 0)
		sums[enc->index]->add(new BinaryIR(getIntType(), 
			getOper(tok_shl), e, new IntIR(enc->start, 0)));
	else
		sums[enc->index]->add(e);
}

void BoolType::produceParam(ExprSum *sums[], ExprIR *floats[], ExprIR *e) {
	IntValue *enc = (IntValue*) enc_;
	if (enc->start != 0)
		sums[enc->index]->add(new BinaryIR(getIntType(), 
			getOper(tok_shl), e, new IntIR(enc->start, 0)));
	else
		sums[enc->index]->add(e);
}

void FloatType::produceParam(ExprSum *sums[], ExprIR *floats[], ExprIR *e) {
	FloatValue *enc = (FloatValue*) enc_;
	floats[enc->index] = e;
}

static TypeEnv *env = new TypeEnv();
void typeChecking(VCProg *prog, bool dump) {
	prog->collectConstraints(env);
	env->check();
	if (dump)
		env->dump();
}

TypeEnv *getTypeEnv() {
	return env;
}

int cmp_type(const Type *t1, const Type *t2) {
	if (t1->type() != t2->type())
		return (int)t1->type() - (int)t2->type();
	if (t1->type() == PalgolTypes::t_list) {
		ListType *l1 = (ListType *) t1;
		ListType *l2 = (ListType *) t2;
		return cmp_type(l1->getContent(), l2->getContent());
	}
	if (t1->type() == PalgolTypes::t_pair || t1->type() == PalgolTypes::t_edge) {
		PairType *p1 = (PairType *) t1;
		PairType *p2 = (PairType *) t2;
		int r1 = cmp_type(p1->getFirst(), p2->getFirst());
		return (r1 != 0 ? r1 : cmp_type(p1->getSecond(), p2->getSecond()));
	}
	return 0;
}

void debug(const Type *t, FILE *f) {
	t->print(f);
	fprintf(f, "\n");
	fflush(f);
}
