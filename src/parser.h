#ifndef PARSER_H
#define PARSER_H

#include "ast.h"
#include "parser.h"

VCProg *parse();
int getPrec(int op); // get the preference

#endif