#include "parser.h"
#include "ast.h"

#include <cstdio>

int main(int argc, char **argv)
{
	if (argc == 1) {
		printf("usage: ./prealgo <filename>\n");
		return 0;
	}
	setInputStream(fopen(argv[1], "r"));
	VCProg *prog = parse();
	debug(prog, stdout);
	return 0;
}
