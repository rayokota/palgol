#ifndef TOKENIZER_H
#define TOKENIZER_H

#include <cstdio>
#include <cstdlib>

#include "errorinfo.h"

enum TokenID : int {
	tok_eof = 1,
	// indent
	tok_lblock,
	tok_rblock,
	// numeric
	tok_int,
	tok_float,
	tok_bool,
	// identifier
	tok_name,
	tok_fname,
	// symbols
	tok_lbracket,
	tok_rbracket,
	tok_lparen,
	tok_rparen,
	tok_lbrace,
	tok_rbrace,
	tok_from, // <-
	tok_to,   // ->
	tok_bar,  // |
	tok_ul,   // _
	tok_V,    //capital V
	tok_id,
	tok_inf,
	tok_local,
	tok_remote,
	tok_stop,
	tok_where,
	tok_inp,  // input
	tok_oup,  // output
	tok_field,
	tok_type,
	tok_extern,
	// modify
	tok_mass, // :=
	tok_mmax, // >?=
	tok_mmin, // <?=
	tok_madd, // +=
	tok_msub, // -=
	tok_mmul, // *=
	tok_mdiv, // /=
	tok_mand, // &=
	tok_mor,  // |=
	tok_mxor, // ^=
	tok_mapp, // list append
	tok_mrem, // list remove
	tok_mrand, // random pick
	tok_marbi, // arbitrary pick
	// operations
	tok_add,
	tok_sub,
	tok_mul,
	tok_div,
	tok_eq,
	tok_ne,
	tok_lt,
	tok_le,
	tok_gt,
	tok_ge,
	tok_and,
	tok_or,
	tok_not,
	tok_bita, // bitwise and
	tok_bito, // bitwise or
	tok_bitn, // bitwise not
	tok_xor,
	tok_shl,
	tok_shr,
	tok_ques,
	tok_colon,
	tok_comma,
	tok_dot,
	tok_bind,
	// keywords
	tok_for,
	tok_in,
	tok_do,
	tok_until,
	tok_fix,
	tok_exists,
	tok_forall,
	tok_repeat,
	tok_end,
	tok_iter,
	tok_range,
	tok_if,
	tok_else,
	tok_let,
	tok_mut,
	tok_agg,
	tok_true,
	tok_false,
	// pair
	tok_fst,
	tok_snd,
	// cast
	tok_toi,
	tok_tof,
	// build_ins
	tok_buildin,
	tok_END
};

struct Token : Pos {
	int type;
	char *val;

	Token():type(0),val(nullptr) {}
	Token(int row, int col, int type, char *val = nullptr) :
			Pos(row, col), type(type), val(val) {}
};

void setInputStream(FILE *f);
void initTokenizer();

Token getNextToken();
const char *getTokenStr(Token tok);
const char *getTokenStr(int type);

#endif
