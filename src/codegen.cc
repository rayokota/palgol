#include <vector>
#include <algorithm>

#include "ast.h"
#include "codegen.h"
#include "config.h"
#include "intermediate.h"
#include "types.h"

using namespace std;

static char buf[128];

static IntValue *getIntValue(IntValue *cur, int bits, MsgInfo *info) {
	int exists = (cur == nullptr ? 0 : cur->start + cur->width);
	if (cur == nullptr || exists + bits > 32) {
		cur = new IntValue();
		cur->index = info->numInt;
		cur->start = 0;
		cur->width = bits;
		info->numInt++;
		return cur;
	} else {
		IntValue *ret = new IntValue();
		ret->index = cur->index;
		ret->start = exists;
		ret->width = bits;
		return ret;
	}

}

static FloatValue *getFloatValue(MsgInfo *info) {
	FloatValue *ret = new FloatValue();
	ret->index = info->numFloat;
	info->numFloat += 1;
	return ret;
}

static int log_two(int value) {
	int ret = 1, s = 2;
	while (value > s) {
		ret += 1;
		s <<= 1;
	}
	return ret;
}

void IntValue::dump(FILE *f) const {
	fprintf(f, "IntValue(index %d, start %d, width %d)\n",
			index, start, width);
}

void FloatValue::dump(FILE *f) const {
	fprintf(f, "FloatValue(index %d)\n", index);
}

string AggregatorManager::add(AggInfo *agg) {
	char ss[10];
	sprintf(ss, "v%d", cnt_++);
	string ret(ss);
	aggs_.insert(make_pair(ss, agg));
	return ss;
}

string AggregatorManager::getDefault() {
	if (aggs_.find(default_) == aggs_.end()) {
		AggFieldIR *fn = new AggFieldIR(getBoolType(), getAggFieldName());
		AggInfo *agg = new AggInfo(getBoolType(), getOper(tok_mor),
				new Conjunction(), fn);
		aggs_.insert(make_pair(default_, agg));
	}
	return default_;
}

FieldName *AggregatorManager::getAggFieldName() {
	return new FieldName("agg");
}

// this function should be invoked outside the aggregator
FieldIR *AggregatorManager::getAggFieldIR() {
	return new FieldIR(getBoolType(), getAggFieldName());
}

AggInfo *AggregatorManager::get(string name) {
	auto v = aggs_.find(name);
	if (v == aggs_.end())
		return nullptr;
	else
		return v->second;
}

bool AggregatorManager::isSimpleAgg() {
	auto v = aggs_.find(default_);
	return (aggs_.size() == 1 && v != aggs_.end());
}

ExprIR *AggregatorManager::getAggIdentity(Type *type, Operator *op) {
	switch (op->type) {
	case tok_mmax:
		if (type->type() == PalgolTypes::t_int ||
			type->type() == PalgolTypes::t_vid)
			return new IntIR(0, 1);
		if (type->type() == PalgolTypes::t_float)
			return new FloatIR(0, 1);
		prtError("type error");
		return nullptr;
	case tok_mmin:
		if (type->type() == PalgolTypes::t_int ||
			type->type() == PalgolTypes::t_vid)
			return new IntIR(0, -1);
		if (type->type() == PalgolTypes::t_float)
			return new FloatIR(0, -1);
		prtError("type error");
		return nullptr;
	case tok_madd:
		if (type->type() == PalgolTypes::t_int ||
			type->type() == PalgolTypes::t_vid)
			return new IntIR(0, 0);
		if (type->type() == PalgolTypes::t_float)
			return new FloatIR(0, 0);
		prtError("type error");
		return nullptr;
	case tok_mand:
		return new BoolIR(true);
	case tok_mor:
		return new BoolIR(false);
	case tok_mxor:
		return new IntIR(0, 0);
	default:
		prtError(*op, "aggregator not supported yet");
		return nullptr;
	}
}

void AggregatorManager::pregel_struct(CodeGen *cg) {
	if (!hasAgg()) return;
	if (isSimpleAgg()) {
		cg->add(new Text("struct XAggValue { bool iter; };"));
		cg->add(new Text());
		return;
	}
	cg->add(new Text("struct XAggValue {"));
	cg->add(new Increase());
	for (auto it : aggs_) {
		Text *text = new Text();
		it.second->type->toCPP(text);
		text->append(" " + it.first + ";");
		cg->add(text);
	}
	cg->add(new Decrease());
	cg->add(new Text("};"));
	cg->add(new Text());
}

void AggregatorManager::pregel_serial(CodeGen *cg) {
	if (!hasAgg() || isSimpleAgg()) return;
	// read
	cg->add(new Text("ibinstream &operator<<(ibinstream &m, const XAggValue &v) {"));
	cg->add(new Increase());
	Text *t1 = new Text("m");
	for (auto it : aggs_)
		t1->append(" << v." + it.first);
	t1->append(";");
	cg->add(t1);
	cg->add(new Text("return m;"));
	cg->add(new Decrease());
	cg->add(new Text("}"));
	cg->add(new Text());
	// write
	cg->add(new Text("obinstream &operator>>(obinstream &m, XAggValue &v) {"));
	cg->add(new Increase());
	Text *t2 = new Text("m");
	for (auto it : aggs_)
		t2->append(" >> v." + it.first);
	t2->append(";");
	cg->add(t2);
	cg->add(new Text("return m;"));
	cg->add(new Decrease());
	cg->add(new Text("}"));
	cg->add(new Text());
}

void AggregatorManager::pregel_body(CodeGen *cg) {
	if (!hasAgg()) return;
	if (isSimpleAgg()) {
		FieldName *fn = getAggFieldName();
		cg->add(new Text("class XAgg : public Aggregator<XVertex, bool, bool> {"));
		cg->add(new Text("private:"));
		cg->add(new Increase());
		cg->add(new Text("bool m;"));
		cg->add(new Decrease());
		cg->add(new Text("public:"));
		cg->add(new Increase());
		cg->add(new Text("void init() { m = false; }"));
		cg->add(new Text("void stepPartial(XVertex *v) { m |= v->value()." + fn->getName() + "; }"));
		cg->add(new Text("void stepFinal(bool *part) { m |= *part; }"));
		cg->add(new Text("bool *finishPartial() { return &m; }"));
		cg->add(new Text("bool *finishFinal() { return &m; }"));
		cg->add(new Decrease());
		cg->add(new Text("};"));
		cg->add(new Text());
		return;
	} else {
		cg->add(new Text("class XAgg : public Aggregator<XVertex, XAggValue, XAggValue> {"));
		cg->add(new Text("private:"));
		cg->add(new Increase());
		cg->add(new Text("XAggValue m;"));
		cg->add(new Decrease());
		cg->add(new Text("public:"));
		cg->add(new Increase());
		// init function
		cg->add(new Text("void init() {"));
		cg->add(new Increase());
		for (auto it : aggs_) {
			AggInfo *agg = it.second;
			VarIR *var = new VarIR(agg->type, "m." + it.first);
			AssignIR *ass = new AssignIR(var, getOper(tok_mass),
					getAggIdentity(agg->type, agg->op));
			ass->pregelPlus(cg);
		}
		cg->add(new Decrease());
		cg->add(new Text("}"));
		cg->add(new Text(""));
		// step partial
		cg->add(new Text("void stepPartial(XVertex *v) {"));
		cg->add(new Increase());
		for (auto it : aggs_) {
			AggInfo *agg = it.second;
			VarIR *var = new VarIR(agg->type, "m." + it.first);
			StmtIR *stmt = agg->conj->produce(
					new AssignIR(var, agg->op, agg->expr));
			stmt->pregelPlus(cg);
		}
		cg->add(new Decrease());
		cg->add(new Text("}"));
		cg->add(new Text(""));
		// step final
		cg->add(new Text("void stepFinal(XAggValue *part) {"));
		cg->add(new Increase());
		for (auto it : aggs_) {
			AggInfo *agg = it.second;
			VarIR *var1 = new VarIR(agg->type, "m." + it.first);
			VarIR *var2 = new VarIR(agg->type, "part->" + it.first);
			AssignIR *ass = new AssignIR(var1, agg->op, var2);
			ass->pregelPlus(cg);
		}
		cg->add(new Decrease());
		cg->add(new Text("}"));
		cg->add(new Text(""));
		// finish partial
		cg->add(new Text("XAggValue *finishPartial() {"));
		cg->add(new Increase());
		cg->add(new Text("return &m;"));
		cg->add(new Decrease());
		cg->add(new Text("}"));
		cg->add(new Text(""));
		// finish final
		cg->add(new Text("XAggValue *finishFinal() {"));
		cg->add(new Increase());
		cg->add(new Text("return &m;"));
		cg->add(new Decrease());
		cg->add(new Text("}"));
		cg->add(new Text(""));
		// ======
		cg->add(new Decrease());
		cg->add(new Text("};"));
		cg->add(new Text());
	}
}

MsgInfo *MessageManager::compute(int flags, SendMsgIR *sm) {
	vector<BasicType*> types = sm->flatten();
	MsgInfo *info = new MsgInfo();
	info->sm = sm;
	IntValue *cur = nullptr;
	if (flags > 1)
		cur = getIntValue(cur, log_two(flags), info);
	vector<MsgEnc*> encs(types.size(), nullptr);
	for (int i = 0; i < types.size(); i++)
		if (types[i]->type() == PalgolTypes::t_bool)
			types[i]->setMsgEnc(cur = getIntValue(cur, 1, info));
	for (int i = 0; i < types.size(); i++)
		if (types[i]->type() == PalgolTypes::t_int ||
			types[i]->type() == PalgolTypes::t_vid)
			types[i]->setMsgEnc(cur = getIntValue(cur, 30, info));
		else if (types[i]->type() == PalgolTypes::t_float)
			types[i]->setMsgEnc(getFloatValue(info));
	return info;
}

void MessageManager::addSM(SendMsgIR *sm) {
	ch2msg_[sm->getChannel()].push_back(sm);
}

SendMsgIR *MessageManager::getFstSM(int ch) {
	return ch2msg_[ch][0];
}

void MessageManager::addChannelToStep(int step, int ch) {
	step2chs_[step].push_back(ch);
	ch2step_[ch] = step;
}

void MessageManager::analysize() {
	for (auto sth : step2chs_) {
		auto channels = sth.second;
		for (auto ch : channels) {
			auto sms = ch2msg_[ch];
			for (auto sm : sms) {
				MsgInfo *info = compute(channels.size(), sm);
				maxInt = max(maxInt, info->numInt);
				maxFloat = max(maxFloat, info->numFloat);
				ch2info_[ch] = info;
			}
		}
	}
	// to decide whether to use combiner
	bool can_decide = true;
	combiner_ = CombinerT::c_null;
	if (!isSimpleType()) {
		can_decide = false;
	} else for (auto sth : ch2msg_) {
		int ch = sth.first;
		SendMsgIR *sm = getFstSM(ch);
		if (can_decide && combiner_ == CombinerT::c_null)
			combiner_ = sm->getCombiner();
		if (!isSimpleType(ch) || combiner_ != sm->getCombiner())
			can_decide = false;
	}
	if (!can_decide || combiner_ == CombinerT::c_rand)
		combiner_ = CombinerT::c_null;
}

bool MessageManager::useCombiner() {
	return combiner_ != CombinerT::c_null;
}

static void listDefs(CodeGen *cg, int num, const char *type) {
	if (num == 0) return;
	Text *text = new Text(string(type));
	for (int i = 0; i < num; i++) {
		sprintf(buf, "%s%c%d", (i == 0 ? " " : ", "), type[0], i);
		text->append(string(buf));
	}
	text->append(";");
	cg->add(text);
}

static void fieldParam(Text *text, int num, const char *s) {
	if (num == 0) return;
	for (int i = 0; i < num; i++) {
		sprintf(buf, "%s%s %c%d", 
				(i > 0 ? ", " : ""), s, s[0], i);
		text->append(string(buf));
	}
}

static void fieldInit(Text *text, int num, const char *s) {
	if (num == 0) return;
	for (int i = 0; i < num; i++) {
		sprintf(buf, "%s%s%d(%s%d)", 
				(i > 0 ? ", " : ""), s, i, s, i);
		text->append(string(buf));
	}
}

bool MessageManager::isUnique(int ch) {
	return step2chs_[ch2step_[ch]].size() == 1;
}

ExprIR *MessageManager::checkFlag(string t, int ch) {
	const vector<int> &ref = step2chs_[ch2step_[ch]];
	int index = 0;
	while (ref[index] != ch) index++;
	int mask = (1 << log_two(ref.size())) - 1;
	if (!isSimpleType())
		t += ".i0";
	VarIR *var = new VarIR(getIntType(), t);
	// if ((var ^ mask) == index)
	return new BinaryIR(getBoolType(), getOper(tok_eq),
		new BinaryIR(getIntType(), getOper(tok_bita),
			var, new IntIR(mask, 0)),
		new IntIR(index, 0));
}

bool MessageManager::isSimpleType() {
	return (maxInt + maxFloat <= 1);
}

std::string MessageManager::getTypeStr() {
	if (isSimpleType()) {
		if (maxFloat == 1)
			return string("double");
		else
			return string("int");
	}
	return string("XMessage");
}

bool MessageManager::isSimpleType(int ch) {
	return isSimpleType() && isUnique(ch);
}

ExprIR *MessageManager::readMessage(int ch, int index, const string &name) {
	if (isSimpleType(ch)) {
		if (maxFloat == 1)
			return new VarIR(getFloatType(), name);
		else
			return new VarIR(getIntType(), name);
	}
	MsgInfo *info = ch2info_[ch];
	Type *ty = info->sm->getType(index);
	return ty->produceRead(name.c_str(), isSimpleType());
}

ExprIR *MessageManager::encMessage(const SendMsgIR *sm) {
	int ch = sm->getChannel();
	if (isSimpleType(ch)) {
		if (sm->size() > 0)
			return sm->get(0);
		else { // handle empty sending list
			if (maxInt == 1)
				return new IntIR(0, 0);
			else
				return new FloatIR(0, 0);
		}
	}
	const vector<int> &ref = step2chs_[ch2step_[ch]];
	int index = 0;
	while (ref[index] != ch) index++;
	// calculate parameters
	ExprSum **sums = new ExprSum*[maxInt];
	ExprIR **floats = new ExprIR*[maxFloat];
	for (int i = 0; i < maxInt; i++)
		sums[i] = new ExprSum();
	for (int i = 0; i < maxFloat; i++)
		floats[i] = nullptr;
	if (index > 0)
		sums[0]->add(new IntIR(index, 0));
	for (int i = 0; i < sm->size(); i++) {
		if (sm->getType(i) == nullptr)
			prtError("runtime error!!!");
		sm->getType(i)->produceParam(sums, floats, sm->get(i));
	}
	// construct return value
	ExprIR *ret = nullptr;
	if (isSimpleType()) {
		if (maxInt == 1)
			ret = sums[0]->produce();
		else
			ret = floats[0];
	} else {
		FunctionIR *func = new FunctionIR(nullptr, string("XMessage"));
		for (int i = 0; i < maxInt; i++)
			func->add(sums[i]->produce());
		for (int i = 0; i < maxFloat; i++)
			if (floats[i] == nullptr)
				func->add(new FloatIR(0, 0));
			else
				func->add(floats[i]);
		ret = func;
	}
	// delete
	for (int i = 0; i < maxInt; i++)
		delete sums[i];
	delete[] sums;
	delete[] floats;
	return ret;
}

void MessageManager::pregel_struct(CodeGen *cg) {
	if (isSimpleType()) return;
	cg->add(new Text("struct XMessage {"));
	cg->add(new Increase());
	listDefs(cg, maxInt, "int");
	listDefs(cg, maxFloat, "double");
	cg->add(new Text("XMessage() {}"));
	Text *text = new Text("XMessage(");
	fieldParam(text, maxInt, "int");
	if (maxInt > 0 && maxFloat > 0)
		text->append(", ");
	fieldParam(text, maxFloat, "double");
	text->append("): ");
	fieldInit(text, maxInt, "i");
	if (maxInt > 0 && maxFloat > 0)
		text->append(", ");
	fieldInit(text, maxFloat, "d");
	text->append(" {}");
	cg->add(text);
	cg->add(new Decrease());
	cg->add(new Text("};"));
	cg->add(new Text());
}

void MessageManager::pregel_serial(CodeGen *cg) {
	if (isSimpleType()) return;
	// read
	cg->add(new Text("ibinstream &operator<<(ibinstream &m, const XMessage &x) {"));
	cg->add(new Increase());
	Text *t1 = new Text("return m");
	for (int i = 0; i < maxInt; i++) {
		sprintf(buf, " << x.i%d", i);
		t1->append(string(buf));
	}
	for (int i = 0; i < maxFloat; i++) {
		sprintf(buf, " << x.d%d", i);
		t1->append(string(buf));
	}
	t1->append(";");
	cg->add(t1);
	cg->add(new Decrease());
	cg->add(new Text("}"));
	cg->add(new Text());
	// write
	cg->add(new Text("obinstream &operator>>(obinstream &m, XMessage &x) {"));
	cg->add(new Increase());
	Text *t2 = new Text("return m");
	for (int i = 0; i < maxInt; i++) {
		sprintf(buf, " >> x.i%d", i);
		t2->append(string(buf));
	}
	for (int i = 0; i < maxFloat; i++) {
		sprintf(buf, " >> x.d%d", i);
		t2->append(string(buf));
	}
	t2->append(";");
	cg->add(t2);
	cg->add(new Decrease());
	cg->add(new Text("}"));
	cg->add(new Text());
}

void MessageManager::pregel_combiner(CodeGen *cg) {
	if (combiner_ == CombinerT::c_null) return;
	Text *text = new Text("class XCombiner : public Combiner<");
	text->append(getTypeStr());
	text->append("> {");
	cg->add(text);
	cg->add(new Text("public:"));
	cg->add(new Increase());
	text = new Text("void combine(");
	text->append(getTypeStr());
	text->append(" &old, const ");
	text->append(getTypeStr());
	text->append(" &new_msg) override {");
	cg->add(text);
	cg->add(new Increase());
	if (combiner_ == CombinerT::c_min)
		cg->add(new Text("old = min(old, new_msg);"));
	if (combiner_ == CombinerT::c_max)
		cg->add(new Text("old = max(old, new_msg);"));
	if (combiner_ == CombinerT::c_sum)
		cg->add(new Text("old += new_msg;"));
	if (combiner_ == CombinerT::c_and)
		cg->add(new Text("old = old && new_msg;"));
	if (combiner_ == CombinerT::c_or)
		cg->add(new Text("old = old || new_msg;"));
	cg->add(new Decrease());
	cg->add(new Text("}"));
	cg->add(new Decrease());
	cg->add(new Text("};"));
	cg->add(new Text());
}

static bool cmp_field(const FieldIR *a, const FieldIR *b) {
	int t = cmp_type(a->type(), b->type());
	return (t != 0 ? t < 0 : a->getName() < b->getName());
}

void VertexManager::add(FieldIR *fn) {
	if (used_.find(fn->getName()) == used_.end()
			&& fn->getName() != "Id") {
		used_.insert(fn->getName());
		fields_.push_back(fn);
	}
}

bool VertexManager::uses(string fn) {
	return used_.find(fn) != used_.end();
}

void VertexManager::pregel_struct(CodeGen *cg) {
	cg->add(new Text("struct XValue {"));
	cg->add(new Increase());
	sort(fields_.begin(), fields_.end(), cmp_field);
	int j = 0;
	for (int i = 0; i < fields_.size(); i = j) {
		Type *ti = fields_[i]->type();
		for (; j < fields_.size() && 
			!cmp_type(fields_[j]->type(), ti); j++);
		Text *text = new Text();
		fields_[i]->type()->toCPP(text);
		text->append(" " + fields_[i]->getName());
		for (int k = i + 1; k < j; k++)
			text->append(", " + fields_[k]->getName());
		text->append(";");
		cg->add(text);
	}
	cg->add(new Decrease());
	cg->add(new Text("};"));
	cg->add(new Text());
}

void VertexManager::pregel_serial(CodeGen *cg) {
	// read
	cg->add(new Text("ibinstream &operator<<(ibinstream &m, const XValue &v) {"));
	cg->add(new Increase());
	int cur = 0, ed;
	for (; cur < fields_.size();) {
		for (ed = cur + 1; ed < fields_.size(); ed++)
			if (cmp_type(fields_[cur]->type(), fields_[ed]->type()) != 0)
				break;
		Text *t1 = new Text("m");
		for (; cur < ed; cur++)
			t1->append(" << v." + fields_[cur]->getName());
		t1->append(";");
		cg->add(t1);
	}
	cg->add(new Text("return m;"));
	cg->add(new Decrease());
	cg->add(new Text("}"));
	cg->add(new Text());
	// write
	cg->add(new Text("obinstream &operator>>(obinstream &m, XValue &v) {"));
	cg->add(new Increase());
	for (cur = 0; cur < fields_.size();) {
		for (ed = cur + 1; ed < fields_.size(); ed++)
			if (cmp_type(fields_[cur]->type(), fields_[ed]->type()) != 0)
				break;
		Text *t1 = new Text("m");
		for (; cur < ed; cur++)
			t1->append(" >> v." + fields_[cur]->getName());
		t1->append(";");
		cg->add(t1);
	}
	cg->add(new Text("return m;"));
	cg->add(new Decrease());
	cg->add(new Text("}"));
	cg->add(new Text());
}

/* Implement the interfaces for class Doc
 *   void print(int lvl, FILE *f);
 */
int Text::print(int lvl, FILE *f) const {
	int spaces = getConfig()->spaces;
	if (spaces > 0) {
		for (int i = 0; i < lvl * spaces; i++)
			fputc(' ', f);
	} else {
		for (int i = 0; i < lvl; i++)
			fputc('\t', f);
	}
	fprintf(f, "%s\n", cont_.c_str());
	return lvl;
}

int Increase::print(int lvl, FILE *f) const {
	return lvl + 1;
}

int Decrease::print(int lvl, FILE *f) const {
	return lvl - 1;
}

/* Implement interfaces for class CodeGen
 */
void CodeGen::setStepSTM(int step, STM *stm) {
	if (stm == nullptr) return;
	stm2step_[stm] = step;
}

int CodeGen::getStepSTM(STM *stm) {
	return stm2step_[stm];
}

void CodeGen::addJump(STM *stm, int curStep) {
	if (stm != nullptr) {
		int nxt = getStepSTM(stm);
		if (nxt != curStep) {
			sprintf(buf, "value().phase = %d;", nxt);
			add(new Text(buf));
		}
	}
}

bool CodeGen::needDef() {
	return need_def_;
}

bool CodeGen::hasDef(VarIR *var) {
	return defs_.find(var->getName()) != defs_.end();
}

void CodeGen::defVar(VarIR *var) {
	defs_.insert(var->getName());
}

void CodeGen::clearDefs() {
	defs_.clear();
}

SeqSTM *CodeGen::checkInput(SeqSTM *prog) {
	TypeEnv *env = getTypeEnv();
	int in = 0, out = 0, nbr = 0;
	for (int i = 0; i < inps_.size(); i++) {
		Type *t = env->getType(inps_[i]);
		if (t->contains(PalgolTypes::t_var)) {
			sprintf(buf, "cannot infer a type for field '%s'",
				inps_[i]->getName().c_str());
			prtError(*inps_[i], buf);
		}
		getVM()->add(new FieldIR(t, inps_[i]));
		in  |= (inps_[i]->getName() == "In");
		out |= (inps_[i]->getName() == "Out");
		nbr |= (inps_[i]->getName() == "Nbr");
	}
	for (int i = 0; i < oups_.size(); i++) {
		Type *t = env->getType(oups_[i]);
		if (t->contains(PalgolTypes::t_var)) {
			sprintf(buf, "cannot infer a type for field '%s'",
				oups_[i]->getName().c_str());
			prtError(*oups_[i], buf);
		}
		getVM()->add(new FieldIR(t, oups_[i]));
	}
	if (nbr && (in || out))
		prtWarning("should not use both In/Out and Nbr");
	if (!getConfig()->genSymEdge || nbr || (in && out))
		return prog;
	if ((!getVM()->uses("In" ) || in ) &&
		(!getVM()->uses("Out") || out))
		return prog;
	FieldName *fi = new FieldName("In");
	FieldName *fo = new FieldName("Out");
	Type *t_nbr = getTypeEnv()->getType(fi);
	Type *t_rec = ((ListType*) t_nbr)->getContent();
	FieldIR *fii = new FieldIR(t_nbr, fi);
	FieldIR *foo = new FieldIR(t_nbr, fo);
	int ch = getIREnv()->newChannel();
	Type *wt = getTypeEnv()->getEdgeWeight(), *et;
	ExprIR *send, *dest, *weig;
	VarIR *e; // sender's variable
	if (wt->equals(getUnitType())) {
		et = getVidType();
		e = new VarIR(et, string("e"));
		dest = e;
		send = new IDIR();
	} else {
		et = new EdgeType(getVidType(), wt);
		e = new VarIR(et, "e");
		dest = new SelectIR(getVidType(), getOper(tok_fst), e);
		weig = new SelectIR(wt, getOper(tok_snd), e);
		send = new PairIR(et, new IDIR(), weig);
	}
	SendMsgIR *sm = new SendMsgIR(ch, 
		vector<ExprIR*>(1, send), dest, CombinerT::c_null);
	ForStmtIR *ss = nullptr;
	ScanStmtIR *rr = nullptr;
	VarIR *var = new VarIR(et, string("msg"));
	if (in && getVM()->uses("Out")) {
		StmtIR *app = new AssignIR(foo, getOper(tok_mapp),
				new ReadMsgIR(t_rec, var->getName(), ch, 0));
		ss = new ForStmtIR("e", fii, sm);
		rr = new ScanStmtIR(ch, var->getName(), nullptr, app);
	}
	if (out && getVM()->uses("In")) {
		StmtIR *app = new AssignIR(fii, getOper(tok_mapp),
				new ReadMsgIR(t_rec, var->getName(), ch, 0));
		ss = new ForStmtIR("e", foo, sm);
		rr = new ScanStmtIR(ch, var->getName(), nullptr, app);
	}
	Superstep *s1 = new Superstep();
	Superstep *s2 = new Superstep();
	s1->add(ss);
	s2->add(rr);
	s2->addChannel(ch); // don't forget!!
	getMM()->addSM(sm); // don't forget!!
	SeqSTM *sec = new SeqSTM(false, s2, nullptr);
	sec->append(prog);
	return new SeqSTM(false, s1, sec);
}

set<string> CodeGen::getInputFields() {
	set<string> ret;
	for (auto fn : inps_)
		ret.insert(fn->getName());
	return ret;
}

void CodeGen::pregel_header() {
	add(new Text("#include \"basic/pregel-dev.h\""));
	add(new Text("#include \"auxiliary.hpp\""));
	if (ext_) {
		sprintf(buf, "#include \"%s\"", getConfig()->external);
		add(new Text(buf));
	}
	add(new Text());
	add(new Text("using namespace std;"));
	add(new Text());
}

void CodeGen::pregel_code(SeqSTM *prog) {
	Text *cls = new Text("class XVertex : public Vertex<int, XValue, ");
	cls->append(getMM()->getTypeStr());
	cls->append("> {");
	add(cls);
	add(new Text("public:"));
	add(new Increase());
	add(new Text("void compute(MessageContainer &msgs) override {"));
	add(new Increase());
	if (getAM()->hasAgg()) {
		add(new Text("const XAggValue *agg = (XAggValue*) getAgg();"));
		defVar(new VarIR(getVidType(), "agg"));
	}
	add(new Text("int phase = value().phase;"));
	defVar(new VarIR(getIntType(), "phase"));
	need_def_ = true;
	prog->pregelPlus(this);
	add(new Decrease());
	add(new Text("}"));
	add(new Decrease());
	add(new Text("};"));
	add(new Text());
	need_def_ = false;
}

void CodeGen::pregel_worker() {
	Text *hd = new Text("class XWorker : public Worker<XVertex");
	if (getAM()->hasAgg()) hd->append(", XAgg");
	hd->append("> {");
	add(hd);
	add(new Text("private:"));
	add(new Increase());
	add(new Text("char buf[120];"));
	add(new Decrease());
	add(new Text("public:"));
	add(new Increase());
	// input
	add(new Text("XVertex *toVertex(char *line) override {"));
	add(new Increase());
	add(new Text("XVertex *v = new XVertex();"));
	add(new Text("Tokenizer tok(line);"));
	for (int i = 0; i < inps_.size(); i++)
		if (inps_[i]->getName() == "Id") {
			add(new Text("parse(&tok, v->id);"));
		} else {
			Text *text = new Text("parse(&tok, v->value().");
			text->append(inps_[i]->getName());
			text->append(");");
			add(text);
		}
    add(new Text("v->value().phase = 1;"));
	add(new Text("return v;"));
	add(new Decrease());
	add(new Text("}"));
	// output
	add(new Text("void toline(XVertex *v, BufferedWriter &writer) override {"));
	add(new Increase());
	add(new Text("OutputBuffer ob(&writer, buf);"));
	for (int i = 0; i < oups_.size(); i++)
		if (oups_[i]->getName() == "Id") {
			add(new Text("print(&ob, v->id);"));
		} else {
			Text *text = new Text("print(&ob, v->value().");
			text->append(oups_[i]->getName());
			text->append(");");
			add(text);
		}
	add(new Text("ob.flush();"));
	add(new Decrease());
	add(new Text("}"));
	add(new Decrease());
	add(new Text("};"));
	add(new Text());
}

void CodeGen::pregel_run() {
	add(new Text("void pregel_run(string in, string out) {"));
	add(new Increase());
	add(new Text("WorkerParams param;"));
	add(new Text("param.input_path = in;"));
	add(new Text("param.output_path = out;"));
	add(new Text("param.force_write = true;"));
	add(new Text("param.native_dispatcher = false;"));
	add(new Text("XWorker worker;"));
	if (getAM()->hasAgg()) {
		add(new Text("XAgg agg;"));
		add(new Text("worker.setAggregator(&agg);"));
	}
	if (getMM()->useCombiner()) {
		add(new Text("XCombiner combiner;"));
		add(new Text("worker.setCombiner(&combiner);"));
	}
	add(new Text("worker.run(param);"));
	add(new Decrease());
	add(new Text("}"));
	add(new Text());
}

void CodeGen::pregel_main() {
	add(new Text("int main(int argc, char* argv[]) {"));
	add(new Increase());
	add(new Text("if (argc < 3) {"));
	add(new Increase());
	add(new Text("fprintf(stderr, \"%s <input_folder> <output_folder>\\n\", argv[0]);"));
	add(new Text("return 0;"));
	add(new Decrease());
	add(new Text("}"));
	add(new Text("init_workers();"));
	add(new Text("pregel_run(argv[1], argv[2]);"));
	add(new Text("worker_finalize();"));
	add(new Text("return 0;"));
	add(new Decrease());
	add(new Text("}"));
}

void CodeGen::dump(FILE *f) {
	int lvl = 0;
	for (auto s : seq_)
		lvl = s->print(lvl, f);
}

static CodeGen *cg = new CodeGen();
CodeGen *getCodeGen() {
	return cg;
}

string rename(string name, int step) {
	sprintf(buf, "S%d_%s", step, name.c_str());
	return string(buf);
}

void genPregelPlus(SeqSTM *prog, FILE *f) {
	cg->setAM(getIREnv()->getAM());
	cg->setMM(getIREnv()->getMM());
	cg->setVM(getIREnv()->getVM());
	prog = cg->checkInput(prog);
	prog->setStep(cg, cg->nextStep());
	cg->pregel_header();
	cg->getVM()->pregel_struct(cg);
	cg->getVM()->pregel_serial(cg);
	MessageManager *mm = cg->getMM();
	mm->analysize();
	mm->pregel_struct(cg);
	mm->pregel_serial(cg);
	mm->pregel_combiner(cg);
	AggregatorManager *am = cg->getAM();
	am->pregel_struct(cg);
	am->pregel_serial(cg);
	cg->pregel_code(prog);
	am->pregel_body(cg);
	cg->pregel_worker();
	cg->pregel_run();
	cg->pregel_main();
	cg->dump(f);
	if (getConfig()->suggest)
		getTypeEnv()->suggestCPP(stderr);
}
