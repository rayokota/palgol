cmake_minimum_required(VERSION 2.6)
project(palgol)

set(src src/tokenizer.cc src/errorinfo.cc src/parser.cc src/ast.cc src/types.cc src/types.cc src/intermediate.cc src/codegen.cc src/config.cc)
set(CMAKE_CXX_FLAGS "-std=c++11")

include_directories(src/)
add_library(pal ${src})

#add_executable(tok_test src/tok_test.cc)
#add_executable(par_test src/par_test.cc)
#add_executable(type_test src/type_test.cc)
add_executable(palgol src/palgol.cc)

#target_link_libraries(tok_test pal)
#target_link_libraries(par_test pal)
#target_link_libraries(type_test pal)
target_link_libraries(palgol pal)

