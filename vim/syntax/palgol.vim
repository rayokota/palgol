if exists("b:current_syntax")
    finish
endif

let b:current_syntax = "palgol"

syntax keyword basicKeywords input output field type extern
syntax keyword basicKeywords local remote stop where
syntax keyword basicKeywords for in V let agg mut
syntax keyword basicKeywords do until fix repeat end iter range
syntax keyword basicKeywords if else exists forall
syntax keyword defaultFields In Out Nbr Id
syntax keyword basicTypes    vid int bool float unit
syntax keyword recordFields  id w size append remove contains
syntax keyword defaultValues inf true false
syntax keyword buildinFuncs  minimum maximum sum and or xor random arbitrary
syntax keyword buildinFuncs  fst snd to_int to_float

syn match commentString "//.*$"
syn match integerString '\d\+'
syn match integerString '[-+]\d\+'
syn match floatString   '\d\+\.\d*'
syn match floatString   '[-+]\d\+\.\d*'
syn match varString     '\l\w*'
syn match fieldString   '\u\w*'

highlight link basicKeywords Keyword
"highlight link recordFields  Keyword
highlight link basicTypes    Type
highlight link defaultValues Constant
highlight link integerString Number
highlight link floatString   Float
"highlight link varString     Identifier
highlight link fieldString   Identifier
highlight link defaultFields Identifier
highlight link buildinFuncs  Keyword
highlight link commentString Comment

