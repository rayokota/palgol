#include "basic/pregel-dev.h"
#include "auxiliary.hpp"

using namespace std;

struct XValue {
    int D, Dup;
    int phase;
    bool agg;
    vector<int> Nbr;
};

ibinstream &operator<<(ibinstream &m, const XValue &v) {
    m << v.D << v.Dup << v.phase << v.agg << v.Nbr;
    return m;
}

obinstream &operator>>(obinstream &m, XValue &v) {
    m >> v.D >> v.Dup >> v.phase >> v.agg >> v.Nbr;
    return m;
}

class XVertex : public Vertex<int, XValue, int> {
public:
    void compute(MessageContainer &msgs) override {
        int phase = value().phase;
        if (phase == 1) {
            value().D = id;
            for (auto e : value().Nbr)
                value().D = min(value().D, e);
            send_message(value().D, id);
            value().Dup = value().D;
            value().agg = true;
            value().phase = 2;
            return;
        }
        if (phase == 2) {
            if ((*(bool*) getAgg())) {
                for (auto msg : msgs)
                    send_message(msg, value().D << 1);
                for (auto e : value().Nbr)
                    send_message(e, 1 + (value().D << 1));
                value().phase = 3;
            } else {
                vote_to_halt();
            }
            return;
        }
        if (phase == 3) {
            int t2;
            for (const auto &msg : msgs)
                if ((msg & 1) == 0)
                    t2 = msg >> 1;
            if (t2 == value().D) {
                int t = INT_MAX;
                for (auto e : msgs)
                    if ((e & 1) == 1)
                        t = min(t, e >> 1);
                if (t < value().D)
                    send_message(value().D, t);
            } else
                value().D = t2;
            value().phase = 4;
            return;
        }
        if (phase == 4) {
            for (auto msg : msgs)
                value().D = min(value().D, msg);
            send_message(value().D, id);
            value().agg = !(value().Dup == value().D);
            value().Dup = value().D;
            value().phase = 2;
            return;
        }
    }
};

class XAgg : public Aggregator<XVertex, bool, bool> {
private:
    bool M;

public:
    void init() {
        M = false;
    }

    void stepPartial(XVertex *v) {
        M = M || v->value().agg;
    }

    void stepFinal(bool *part) {
        M = M || *part;
    }

    bool *finishPartial() {
        return &M;
    }

    bool *finishFinal() {
        return &M;
    }
};

class XWorker : public Worker<XVertex, XAgg> {
private:
    char buf[120];
public:
    XVertex *toVertex(char *line) override {
        XVertex *v = new XVertex();
        Tokenizer tok(line);
        parse(&tok, v->id);
        parse(&tok, v->value().Nbr);
        v->value().phase = 1;
        return v;
    }
    void toline(XVertex *v, BufferedWriter &writer) override {
        OutputBuffer ob(&writer, buf);
        print(&ob, v->id);
        print(&ob, v->value().D);
        ob.flush();
    }
};

void pregel_run(string in, string out) {
    WorkerParams param;
    param.input_path = in;
    param.output_path = out;
    param.force_write = true;
    param.native_dispatcher = false;
    XWorker worker;
    XAgg agg;
    worker.setAggregator(&agg);
    worker.run(param);
}

int main(int argc, char* argv[]) {
    if (argc < 3) {
        fprintf(stderr, "%s <input_folder> <output_folder>\n", argv[0]);
        return 0;
    }
    init_workers();
    pregel_run(argv[1], argv[2]);
    worker_finalize();
    return 0;
}
