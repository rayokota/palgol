// shortest path with unit edge weight!! equivalent to a BFS

#include "basic/pregel-dev.h"
#include "auxiliary.hpp"
using namespace std;

struct XValue {
    int dist;
    vector<int> edges;
};

ibinstream & operator<<(ibinstream & m, const XValue & v) {
    m<< v.dist << v.edges;
    return m;
}

obinstream & operator>>(obinstream & m, XValue & v) {
    m >> v.dist >> v.edges;
    return m;
}

class XVertex:public Vertex<VertexID, XValue, int> {
public:
    virtual void compute(MessageContainer & messages) {
        int minDist = (id == 1 ? 0 : INT_MAX);
        for (int i = 0; i < messages.size(); i++)
            minDist = min(minDist, messages[i]);
        if (minDist < value().dist) {
            value().dist = minDist;
            for (int i = 0; i < value().edges.size(); i++)
                send_message(value().edges[i], value().dist + 1);
        }
        vote_to_halt();
    }
};

class SSSPWorker:public Worker<XVertex> {
private:
    char buf[1000];

public:
    virtual XVertex* toVertex(char* line)
    {
        Tokenizer tok(line);
        XVertex *v = new XVertex();
        parse(&tok, v->id);
        parse(&tok, v->value().edges);
        v->value().dist = INT_MAX;
        return v;
    }

    virtual void toline(XVertex* v, BufferedWriter & writer) {
        if (v->value().dist != INT_MAX)
        	sprintf(buf, "%d\t%d\n", v->id, v->value().dist);
        else
        	sprintf(buf, "%d\tunreachable\n", v->id);
        writer.write(buf);
    }
};

class SSSPCombiner:public Combiner<int> {
public:
    virtual void combine(int &old, const int &new_msg) {
        if(old > new_msg)
            old = new_msg;
    }
};

void pregel_sssp(string in_path, string out_path){
    WorkerParams param;
    param.input_path=in_path;
    param.output_path=out_path;
    param.force_write=true;
    param.native_dispatcher=false;
    SSSPWorker worker;
    SSSPCombiner combiner;
    worker.setCombiner(&combiner);
    worker.run(param);
}

int main(int argc, char* argv[]){
    if (argc < 3) {
        fprintf(stderr, "insufficient arguments\n");
        return 0;
    }
    init_workers();
    pregel_sssp(argv[1], argv[2]);
    worker_finalize();
    return 0;
}
