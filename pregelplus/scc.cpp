#include "basic/pregel-dev.h"
#include "auxiliary.hpp"

using namespace std;

struct XValue {
    int phase, scc, fwd, bwd;
    bool finish;
    vector<int> in, out, rmi, rmo;
};

ibinstream & operator<<(ibinstream & m, const XValue & v) {
    m << v.phase << v.scc << v.fwd << v.bwd << v.finish;
    m << v.in << v.out << v.rmi << v.rmo;
    return m;
}

obinstream & operator>>(obinstream & m, XValue & v) {
    m >> v.phase >> v.scc >> v.fwd >> v.bwd >> v.finish;
    m >> v.in >> v.out >> v.rmi >> v.rmo;
    return m;
}

// es: an edge list of a vertex
// rm: a list of vertex id that need to be removed 
void removeEdges(vector<int> &es, vector<int> &rm) {
    if (es.empty() || es.size() == rm.size()) {
        es.clear();
        rm.clear();
        return;
    }
    sort(rm.begin(), rm.end());
    int ptr = 0;
    vector<int> ret;
    for (int i = 0; i < es.size(); i++)
        if (ptr >= rm.size() || es[i] != rm[ptr])
            ret.push_back(es[i]);
        else
            ptr++;
    es.swap(ret);
    rm.clear();
}

class XVertex:public Vertex<VertexID, XValue, int> {
public:
    virtual void compute(MessageContainer & msgs) {
        int phase = value().phase;
        if (phase == 1) {
            value().scc = -1;
            sort(value().out.begin(), value().out.end());
            for (int e : value().out)
                send_message(e, id);
            value().phase = 2;
            return;
        }
        if (phase == 2) {
            value().in.swap(msgs);
            sort(value().in.begin(), value().in.end());
            bool trivial = value().in.empty() || value().out.empty();
            value().finish = true;
            if (trivial) {
                value().scc = id;
                for (int e : value().in)
                    send_message(e, id << 1);
                for (int e : value().out)
                    send_message(e, 1 + (id << 1));
                value().in.clear();
                value().out.clear();
                value().finish = false;
            }
            value().phase = 3;
            return;
        }
        if (phase == 3) {
            if (!*(bool*) getAgg()) {
                for (int m : msgs)
                    if ((m & 1) == 1)
                        value().rmi.push_back(m >> 1);
                    else
                        value().rmo.push_back(m >> 1);
                if (value().scc != -1) {
                    value().finish = true;
                    return;
                }
                bool trivial = (value().rmi.size() == value().in.size()
                        || value().rmo.size() == value().out.size());
                value().finish = true;
                if (trivial) {
                    value().scc = id;
                    for (int e : value().in)
                        send_message(e, id << 1);
                    for (int e : value().out)
                        send_message(e, 1 + (id << 1));
                    value().in.clear();
                    value().out.clear();
                    value().finish = false;
                }
            } else {
                if (!value().rmi.empty())
                    removeEdges(value().in, value().rmi);
                if (!value().rmo.empty())
                    removeEdges(value().out, value().rmo);
                if (value().scc != -1) { // permanently sleep
                    vote_to_halt();
                    return;
                }
                value().fwd = id;
                for (int e : value().out)
                    send_message(e, value().fwd);
                value().finish = false;
                value().phase = 4;
            }
            return;
        }
        if (phase == 4) {
            if (!*(bool*) getAgg()) {
                int minv = INT_MAX;
                for (int m : msgs)
                    minv = min(minv, m);
                value().finish = true;
                if (minv < value().fwd) {
                    value().fwd = minv;
                    value().finish = false;
                    for (int e : value().out)
                        send_message(e, value().fwd);
                }
            } else {
                if (value().fwd == id) {
                    value().bwd = id;
                    for (int e : value().in)
                        send_message(e, value().bwd);
                    value().finish = false;
                } else {
                    value().bwd = INT_MAX;
                    value().finish = true;
                }
                value().phase = 5;
            }
            return;
        }
        if (phase == 5) {
            if (!*(bool*) getAgg()) {
                int minv = INT_MAX;
                for (int m : msgs)
                    minv = min(minv, m);
                value().finish = true;
                if (minv < value().bwd) {
                    value().bwd = minv;
                    value().finish = false;
                    for (int e : value().in)
                        send_message(e, value().bwd);
                }
            } else {
                value().finish = true;
                if (value().fwd == value().bwd) {
                    value().scc = value().fwd;
                    for (int e : value().in)
                        send_message(e, id << 1);
                    for (int e : value().out)
                        send_message(e, 1 + (id << 1));
                    value().in.clear();
                    value().out.clear();
                    value().finish = false;
                }
                value().phase = 3;
            }
            return;
        }
    }
};

class XAgg : public Aggregator<XVertex, bool, bool> {
private:
    bool M;
public:
    void init() { M = true; }
    void stepPartial(XVertex *v) { M = M && v->value().finish; }
    void stepFinal(bool *part) { M = M && *part; }
    bool *finishPartial() { return &M; }
    bool *finishFinal() { return &M; }
};

class SCCWorker:public Worker<XVertex, XAgg> {
private:
    char buf[120];

public:
    virtual XVertex* toVertex(char* line) {
        Tokenizer tok(line);
        XVertex *v = new XVertex();
        parse(&tok, v->id);
        parse(&tok, v->value().out);
        v->value().phase = 1;
        return v;
    }

    virtual void toline(XVertex* v, BufferedWriter & writer) {
        OutputBuffer ob(&writer, buf);
        print(&ob, v->id);
        print(&ob, v->value().scc);
        ob.flush();
    }
};

void pregel_scc(string in_path, string out_path){
    WorkerParams param;
    param.input_path=in_path;
    param.output_path=out_path;
    param.force_write=true;
    param.native_dispatcher=false;
    SCCWorker worker;
    XAgg agg;
    worker.setAggregator(&agg);
    worker.run(param);
}

int main(int argc, char* argv[]){
    if (argc < 3) {
        fprintf(stderr, "insufficient arguments\n");
        return 0;
    }
    init_workers();
    pregel_scc(argv[1], argv[2]);
    worker_finalize();
    return 0;
}
