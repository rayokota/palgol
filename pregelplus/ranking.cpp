#include "basic/pregel-dev.h"
#include "utils/type.h"
#include "auxiliary.hpp"

using namespace std;

struct RankingValue {
	int Pred, Sum;
};

ibinstream & operator<<(ibinstream & m, const RankingValue &v){
	m << v.Sum << v.Pred;
	return m;
}

obinstream & operator>>(obinstream & m, RankingValue & v){
	m >> v.Sum >> v.Pred;
	return m;
}

//====================================

class RankingVertex : public Vertex<int, RankingValue, int> {
public:
	virtual void compute(MessageContainer & messages) {
		if (step_num() == 1) {
			if (value().Pred != id)
				send_message(value().Pred, id);
		} else
		if (step_num() % 2 == 0) {
			for (int i = 0; i < messages.size(); i++) {
				send_message(messages[i], value().Pred << 1);
				send_message(messages[i], (value().Sum << 1) + 1);
			}
			vote_to_halt();
		} else {
		    int pred, sum;
			for (int i = 0; i < messages.size(); i++)
				if (messages[i] & 1)
					sum = messages[i] >> 1;
				else
					pred = messages[i] >> 1;
			if (pred != value().Pred) {
				value().Pred = pred;
				value().Sum  = sum;
				send_message(pred, id);
			}
			vote_to_halt();
		}
	}
};


class RankingWorker : public Worker<RankingVertex> {
	char buf[100];
public:

	virtual RankingVertex* toVertex(char* line) {
		Tokenizer tok(line);
		RankingVertex *v = new RankingVertex();
		parse(&tok, v->id);
		parse(&tok, v->value().Pred);
		parse(&tok, v->value().Sum);
		return v;
	}

	virtual void toline(RankingVertex* v, BufferedWriter & writer) {
		sprintf(buf, "%d%d\n", v->id, v->value().Sum);
		writer.write(buf);
	}
};

void pregel_run(string in_path, string out_path){
	WorkerParams param;
	param.input_path=in_path;
	param.output_path=out_path;
	param.force_write=true;
	param.native_dispatcher=false;
	RankingWorker worker;
	worker.run(param);
}

int main(int argc, char* argv[]){
    if (argc < 3) {
        fprintf(stderr, "insufficient arguments\n");
        return 0;
    }
	init_workers();
	pregel_run(argv[1], argv[2]);
	worker_finalize();
	return 0;
}
