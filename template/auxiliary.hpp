#ifndef AUXILIARY_HPP
#define AUXILIARY_HPP

#include <vector>

#include "basic/pregel-dev.h"

class Tokenizer {
private:
	const char *str_;
	int p_;
public:
	Tokenizer(const char *str) : str_(str), p_(0) {}
	
	int getInt() {
        int neg = 0;
		while (isspace(str_[p_])) p_++;
        if (str_[p_] == '-') { p_++; neg=1; }
		int ret = atoi(str_ + p_);
		while (isdigit(str_[p_])) p_++;
		return (neg ? -ret : ret);
	}

	bool getBool() {
		while (isspace(str_[p_])) p_++;
		bool ret = (!strncmp(str_ + p_, "true", 4) || atoi(str_ + p_) != 0);
		while (isalnum(str_[p_])) p_++;
		return ret;
	}

	double getFloat() {
        int neg = 0;
		while (isspace(str_[p_])) p_++;
        if (str_[p_] == '-') { p_++; neg=1; }
		double ret = atof(str_ + p_);
		while (isdigit(str_[p_]) || str_[p_] == '.') p_++;
		return (neg ? -ret : ret);
	}
};

void parse(Tokenizer *t, int &ret) {
	ret = t->getInt();
}

void parse(Tokenizer *t, bool &ret) {
	ret = t->getBool();
}

void parse(Tokenizer *t, double &ret) {
	ret = t->getFloat();
}

void parse(Tokenizer *t, size_t &ret) {
	ret = t->getInt();
}

template<typename T1, typename T2>
void parse(Tokenizer *t, std::pair<T1, T2> &ret) {
	parse(t, ret.first);
	parse(t, ret.second);
}

template<typename T>
void parse(Tokenizer *t, std::vector<T> &ret) {
	int l;
	parse(t, l);
	T x;
	for (int i = 0; i < l; i++) {
		parse(t, x);
		ret.push_back(x);
	}
}

class OutputBuffer {
private:
	BufferedWriter *writer_;
    char *buf_;
    int p_;
public:
    OutputBuffer(BufferedWriter *writer, char *buf) :
    		writer_(writer), buf_(buf), p_(0) {
        buf_[0] = '\n';
        buf_[1] = 0;
    }
    
    void flush() {
    	writer_->write(buf_);
        p_ = 0;
    }

    char *getStr() {
    	return buf_;
    }

    void addInt(int val) {
    	if (p_ != 0) buf_[p_++] = ' ';
        if (p_ > 100) flush();
        sprintf(buf_ + p_, "%d", val);
        while (buf_[p_]) p_++;
        buf_[p_] = '\n';
        buf_[p_ + 1] = 0;
    }

    void addBool(bool val) {
    	if (p_ != 0) buf_[p_++] = ' ';
        if (p_ > 100) flush();
    	buf_[p_++] = (val ? '1' : '0');
        buf_[p_] = '\n';
        buf_[p_ + 1] = 0;
    }

    void addFloat(double val) {
    	if (p_ != 0) buf_[p_++] = ' ';
        if (p_ > 100) flush();
        sprintf(buf_ + p_, "%.4f", val);
        while (buf_[p_]) p_++;
        buf_[p_] = '\n';
        buf_[p_ + 1] = 0;
    }
};

void print(OutputBuffer *t, int val) {
	t->addInt(val);
}

void print(OutputBuffer *t, bool val) {
	t->addBool(val);
}

void print(OutputBuffer *t, double val) {
	t->addFloat(val);
}

void print(OutputBuffer *t, size_t val) {
	t->addInt(val);
}

template<typename T1, typename T2>
void print(OutputBuffer *t, std::pair<T1, T2> &ret) {
	print(t, ret.first);
	print(t, ret.second);
}

template<typename T>
void print(OutputBuffer *t, std::vector<T> &ret) {
	print(t, ret.size());
	for (int i = 0; i < ret.size(); i++)
		print(t, ret[i]);
}

template<typename T1, typename T2>
ibinstream &operator<<(ibinstream &m, const pair<T1, T2> &v) {
    m << v.first << v.second;
    return m;
}

template<typename T1, typename T2>
obinstream &operator>>(obinstream &m, pair<T1, T2> &v) {
    m >> v.first >> v.second;
    return m;
}

template<typename T>
pair<T, int> random(pair<T, int> v1, pair<T, int> v2) {
    int sum = v1.second + v2.second;
    if (sum == 0)
        return v1;
    else {
        int x = rand() % sum;
        if (x < v1.second)
            return make_pair(v1.first, sum);
        else
            return make_pair(v2.first, sum);
    }
}

template<typename T>
pair<T, double> random(pair<T, double> v1, pair<T, double> v2) {
    double sum = v1.second + v2.second;
    if (sum < 1e-8)
        return v1;
    else {
        double x = rand() * sum / RAND_MAX;
        if (x < v1.second)
            return make_pair(v1.first, sum);
        else
            return make_pair(v2.first, sum);
    }
}

template<typename T>
bool contains(const std::vector<T> vec, const T &val) {
    return std::find(vec.begin(), vec.end(), val) != vec.end();
}

#endif
